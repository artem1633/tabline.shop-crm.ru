<?php

namespace app\controllers;

use Yii;
use app\models\blocks\MapMarks;
use app\models\blocks\MapMarksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\blocks\MapBlock;

/**
 * MapMarksController implements the CRUD actions for MapMarks model.
 */
class MapMarksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new MapMarks model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $request = Yii::$app->request;
        $model = new MapMarks();
        $model->map_id = $id;
        $mapMark = MapMarks::find()->where(['map_id' => $id])->orderBy(['id' => SORT_DESC])->one();
        if($mapMark == null) {
            $model->coordinate_x = $model->map->getCoordinateX();  
            $model->coordinate_y = $model->map->getCoordinateY();
            $model->color = '#ff0000';
            $model->title = 'Москва';
        }else{
            $model->coordinate_x = $mapMark->coordinate_x;  
            $model->coordinate_y = $mapMark->coordinate_y;
            $model->color = $mapMark->color;
            $model->title = $mapMark->title;
        }

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['/map-block/update', 'id' => $id]);
        }else{
            return $this->render('/blocks/map-marks-block/create', [
                'model' => $model,
            ]);
        }
       
    }

    /**
     * Updates an existing MapMarks model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['/map-block/update', 'id' => $model->map_id]);
        }else{
            return $this->render('/blocks/map-marks-block/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete an existing MapMarks model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#map-pjax'];
    }

    /**
     * Finds the MapMarks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MapMarks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MapMarks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
