<?php

namespace app\controllers;

use Yii;
use app\models\Products;
use app\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Directory;
use app\models\GoodsCatalogSearch;
use app\models\UsersPage;
use app\models\additional\ProductOptions;
use app\models\additional\Columns;
use app\models\additional\UploadForm;
use yii\web\UploadedFile;
use app\models\Users;
use app\models\additional\Common;
use app\models\Template;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'import', 'send', 'view', 'create', 'update', 'upload', 'sorting', 'delete'],
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionSortCategory()
    {
        $common = Common::find()->where(['user_id' => Yii::$app->user->identity->id ])->one();
        $common->html_code = '1'.$_POST['sort'];
        $common->save();
    }

    // Записать сессию продукт.
    public function actionSetProduct($product_id, $count)
    {
        $session = Yii::$app->session;
        $array = [];
        $q = 0;
        if(!$session['list'] == null) $array  = $session['list'];

        foreach ($array as $key => $value) {
            if( $value['product_id'] == $product_id) {
                $array[$key]['count'] += $count;
                $q = 1;
            }
        }
        if($q == 0) {            
            $array [] = [
                'product_id' => $product_id,
                'count' => $count,
            ];
        }
        
        $session['list'] = $array;        
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,Yii::$app->request->post());

        $searchArchives = new ProductsSearch();
        $archives = $searchArchives->searchArchives(Yii::$app->request->queryParams,Yii::$app->request->post());

        $catalogSearch = new GoodsCatalogSearch();
        $catalogs = $catalogSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'catalogSearch' => $catalogSearch,
            'catalogs' => $catalogs,
            'archives' => $archives,
            'searchArchives' => $searchArchives,
            'post' => Yii::$app->request->post(),
        ]);
    }

    //Импортировать продуктов.
    public function actionImport() 
    {
        $model = new UploadForm();
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            try {
                    $model->file = UploadedFile::getInstance($model, 'file');
                    if($model->file == null) {
                        return [
                            'title'=> "<span style='color:red;'>Сначала выберите файл</span>",
                            'size' => 'normal',
                            'content'=>$this->renderAjax('import_form', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                        Html::button('Импорт',['class'=>'btn btn-primary','type'=>"submit"])
                        ]; 
                    }
                    $fileName = $model->file->baseName . '.' . $model->file->extension;

                    if ($model->load($request->post())) {
                        $model->file = UploadedFile::getInstance($model, 'file');
                        $model->file->saveAs('uploads/' . $fileName);
                    }
                    
                    $filePath = 'uploads/' . $fileName;
                    $csvData = \PHPExcel_IOFactory::load($filePath);
                    $csvData = $csvData->getActiveSheet()->toArray(null, true, true, false);

                    $count = 0;
                    foreach ($csvData as $a) {
                        if ($count==0) {
                            $count ++;
                            continue;
                        }

                        $old_product = Products::find()->where(['name' => $a[0], 'user_id' => Yii::$app->user->identity->id,  ])->one();
                        if($old_product == null) { 
                            $product = new Products();
                            $product->user_id = Yii::$app->user->identity->id;
                            $product->price = $a[1];
                            $product->name = $a[0];
                            $product->weight = $a[2];
                            $product->delivery = 0;
                            $product->view_in_catalog = 0;
                            $product->status = 'active';
                            $product->save();

                            $page = UsersPage::find()->where(['user_id' => Yii::$app->user->identity->id, 'active' => 1])->one();
                            $product->link = "http://". $_SERVER['SERVER_NAME'] ."/" . $page->link_name . "/m/product/" . $product->id;
                            $product->save();
                        }else{
                            $old_product->price = $a[1];
                            $old_product->weight = $a[2];
                            $old_product->save();
                        }              

                        $count++;
                    }

                    return [
                        'title'=> "Импорт",
                        'size' => 'normal',
                        'forceClose'=>true,
                        'forceReload'=>'#crud-datatable-pjax'
                    ]; 
                }
            catch (Exception $exception) {
                return $exception;
            }
        }else{ 
            return [
                'title'=> "Импорт",
                'size' => 'normal',
                'content'=>$this->renderAjax('import_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Импорт',['class'=>'btn btn-primary','type'=>"submit"])
            ]; 

        }
    }

    //Скачать шаблон excell файл.
    public function actionSend()
    {
        return \Yii::$app->response->sendFile("examples/Шаблон товаров.xlsx");
    }


    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $product = $this->findModel($id);
        $user = Users::findOne($product->user_id);
        $request = Yii::$app->request;
        return $this->render('view', [
            'product' => $product,
            'user' => $user,
        ]);
    }

    /**
     * Creates a new Products model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Products();
        $model->status = 'active';
        $number = Products::getLastId();
        $targetDir = Directory::createProductsDirectory($number);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $page = UsersPage::find()->where(['user_id' => Yii::$app->user->identity->id, 'active' => 1])->one();
                $model->link = "http://". $_SERVER['SERVER_NAME'] ."/" . $page->link_name . "/m/product/" . $model->id;
                $model->save();

                $post = $request->post();
                $i=-1;
                foreach ($post['titles'] as $value) {
                    $i++;
                    $option = new ProductOptions();
                    $option->name = $value;
                    $option->price = $post['prices'][$i];
                    $option->product_id = $model->id;
                    $option->save();
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Товар",
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Товар",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'number' => $number,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'number' => $number,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        $number = $id;
        $options = ProductOptions::find()->where(['product_id' => $id])->all();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $post = $request->post();
                //удаление слайды из бд
                foreach ($options as $option) {
                    $isset = 0;
                    foreach ($post['option_id'] as $key => $value) {
                        if( $value == $option->id ) $isset = 1;
                    }
                    if($isset == 0) $option->delete();
                }

                $max_number = -1;
                foreach ($post['option_id'] as $key => $value) {
                    $max_number = $key;
                    $option = ProductOptions::findOne($value);
                    if($option != null){
                        $option->name = $post['titles'][$key];
                        $option->price = $post['prices'][$key];
                        $option->product_id = $model->id;
                        $option->save();
                    }
                }

                $i = -1;
                if(ProductOptions::find()->where(['product_id' => $model->id])->one() == null) $max_number = -1;
                foreach ($post['titles'] as $key => $value) {
                    $i++;
                    if($key > $max_number) {
                        $product = new ProductOptions();
                        $product->product_id = $model->id;
                        $product->name = $post['titles'][$i];
                        $product->price = $post['prices'][$i];
                        $product->save();
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Товар",
                    'forceClose'=>true,
                ];    
            }else{
                return [
                    'title'=> "Товар",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'number' => $number,
                        'options' => $options,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'number' => $number,
                    'options' => $options,
                ]);
            }
        }
    }

    //Страница Магазин ползователя
    public function actionShop($user_id)
    {
        $products = Products::find()->where(['user_id' => $user_id])->orderBy(['catalog_id' => SORT_ASC])->all();
        $user = Users::findOne($user_id);
        return $this->render('shop', [
            'products' => $products,
            'user' => $user,
        ]);

    }

    //Скачать картинки товара на сервер.
    public function actionUpload($number)
    {
        $uploadPath = 'images/products/'.$number;

        if (isset($_FILES['image'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('image');
            $original_name = $file->baseName;  
            $newFileName = $original_name.'.'.$file->extension;
            // you can write save code here before uploading.
            if ($file->saveAs($uploadPath . '/' . $newFileName)) {
            }
        }
        else {
            return $this->render('create', [
                'model' => new Products(),
            ]);
        }
    }

    // Удаление картинок товара из сервера.
    public function actionRemoveFile($id, $filename)
    {
        if (file_exists('images/products/' . $id . '/' . $filename))  {
            unlink(Yii::getAlias('images/products/' . $id . '/' . $filename));
        }
    }

    //Загружение картинки товара.
    public function actionSetImage($id, $photos)
    {
        $product = Products::findOne($id);
        if($product != null) {
            $product->photos = $photos;
            $product->save();
        }
    }

    // Упаковка товаров на корзинку.
    public function actionGetProductCart($id,$product_id)
    {
        $product = Products::findOne($id);
        if($product != null) {
            return $this->renderPartial('_product_view', [ 
                'product' => $product, 
                'product_id' => $product_id
            ]);
        }
        else return '';
    }

    //Сортировка график полей товаров.
    public function actionSorting()
    {
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->identity->id;       

        if ($request->post()) {
            $post = $request->post();
            //aktiv columnlarni bazaga yozilishi
            $active = $post['active'];
            $strlen = strlen( $active );
            $id = ''; $order_number = 0;
            for( $i = 0; $i <= $strlen; $i++ ) {
                $char = substr( $active, $i, 1 );             
                if($char == ',') {
                    $order_number++;
                    Yii::$app->db->createCommand()->update('columns',
                        [
                            'status' => 1, 
                            'order_number' => $order_number 
                        ], 
                        [ 'id' => $id  ])
                    ->execute();
                    $id = '';
                }
                else $id .= $char;
            }
            $order_number++;
            Yii::$app->db->createCommand()->update('columns', 
                [
                    'status' => 1, 
                    'order_number' => $order_number 
                ], 
                [ 'id' => $id ])
            ->execute();

            //aktiv bolmagan columnlarni bazaga yozilishi
            $noactive = $post['no-active'];
            $strlen = strlen( $noactive );
            $id = ''; 
            for( $i = 0; $i <= $strlen; $i++ ) {
                $char = substr( $noactive, $i, 1 );             
                if($char == ',') {
                    Yii::$app->db->createCommand()->update('columns', ['status' => 0 ], [ 'id' => $id ])->execute();
                    $id = '';
                }
                else $id .= $char;
            }
            Yii::$app->db->createCommand()->update('columns', ['status' => 0 ], [ 'id' => $id ])->execute();

            return $this->redirect(['/products/index']);
        }else{
            $column = Columns::find()->where(['user_id' => $user_id, 'table_name' => 'products'])->one();
            if($column == null ) Products::setDefaultValues($user_id);

            $columns = Columns::find()
                ->where([
                    'user_id' => $user_id, 
                    'status' => 1, 
                    'table_name' => 'products'
                ])
                ->orderBy([ 'order_number' => SORT_ASC ])
                ->all();
            $active = []; $i=0;
            foreach ($columns as $column) {
                $i++;
                $name = Template::getColumnsTemplate($i, $column->name);
                $active += [
                    $column->id => ['content' => $name],
                ];
            }

            $columns = Columns::find()->where(['user_id' => $user_id, 'status' => 0, 'table_name' => 'products'])->all();
            $noactive = []; $i=0;
            foreach ($columns as $column) {
                $i++;
                $name = Template::getColumnsTemplate($i, $column->name);
                $noactive += [
                    $column->id => ['content' => $name],
                ];
            }
            
            return $this->render('sorting',[
                'active' => $active,
                'noactive' => $noactive,
            ]);
        }
    }

    /**
     * Delete an existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $product =  $this->findModel($id);

        $options = ProductOptions::find()->where(['product_id' => $id])->all();
        foreach ($options as $value) {
            $value->delete();
        }

        $data = json_decode($product->photos, true);
        foreach ($data as $value) {
            if (file_exists($value['path']))  unlink(Yii::getAlias($value['path']));
        }
        $product->delete();

        if($request->isAjax){            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
