<?php

namespace app\controllers;

use Yii;
use app\models\additional\Common;
use app\models\additional\CommonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\additional\LegalInformation;
use app\models\additional\Tariffs;
use app\models\Users;
use app\models\additional\Faq;
use app\models\additional\Write;
use app\models\additional\Legal;
use app\models\additional\Blogs;
use app\models\additional\Permissions;
use app\models\additional\PromoCodes;
use yii\web\UploadedFile;

/**
 * CommonController implements the CRUD actions for Common model.
 */
class CommonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Страница “Настройки”.
     * @return mixed
     */
    public function actionIndex()
    { 
        $user_id = Yii::$app->user->identity->id;
        $legal_information = LegalInformation::find()->where(['user_id' => $user_id])->one();
        $common = Common::find()->where(['user_id' => $user_id])->one();

        return $this->render('/additional/common/index', [
            'common' => $common,
            'dataProvider' => $dataProvider,
            'legal_information' => $legal_information,
            'user_id' => $user_id,
        ]);
    }

    // Страница Вопросы и ответы
    public function actionFaq()
    {
        $faq = Faq::find()->where(['status' => 1])->all();
        return $this->render('/additional/common/faq', [
            'faq' => $faq,
        ]);
    }

    // Страница Цены и тарифы
    public function actionPriceList()
    { 
        $user_id = Yii::$app->user->identity->id;
        $user = Users::findOne($user_id);
        $free = Tariffs::find()->where(['key' => 'free' ])->one();
        $pro = Tariffs::find()->where(['key' => 'pro' ])->one();
        $data = json_decode($pro->price_list,true);

        $session = Yii::$app->session;
        $session['prices'] = [
            'key' => $pro->key,
            'price' => $data[0]['price'],
            'time' => $data[0]['time'],
            'index' => 0,
        ];

        return $this->render('/additional/common/price-list', [
            'tariff' => $tariff,
            'free' => $free,
            'pro' => $pro,
            'user_id' => $user_id,
            'user' => $user,
        ]);
    }

    // Страница “Цены и тарифы” для администратора.
    public function actionTariffsList()
    { 
        $tariffs = Tariffs::find()->all();
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        $dostup = true;

        if(Yii::$app->user->identity->id == null) $dostup = false;
        if($type == 2) {
            $permission = Permissions::find()->where(['user_id' => $user_id])->one();
            if(!$permission->change_price_list) $dostup = false;
        }

        return $this->render('/additional/common/tariffs-list', [
            'tariffs' => $tariffs,
            'dostup' => $dostup,
        ]);
    }

    // Изменить тарифа
    public function actionChangeTariff()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $session = Yii::$app->session;

        $user = Users::findOne(Yii::$app->user->identity->id);
        $user->last_payment_date = date('Y-m-d');
        $user->tariff_id = Tariffs::find()->where(['key' => $session['prices']['key'] ])->one()->id;
        $user->end_access = date('Y-m-d', strtotime( $session['prices']['time'] . " month"));
        $user->total_payment_amount += $session['prices']['price'];
        $user->save();
        
        Common::sendMessageToEmail(
            $user->login, 
            'Уважаемый(ая) '.$user->fio.'!<br>'. 
            'Вы сменили (продлили) свой тарифный план успешно. Срок действие вашего тарифного плана до' . 
            date('d.m.Y', strtotime( $session['prices']['time'] . " month")), 
            'Cмена тарифного плана'
        );

        return [
            'title' => 'Изменить тариф',
            'size' => 'normal',
            'content'=>'<span style="text-align:center;font-size:16px;color:blue;font-weight:bold;">Успешно выполнено</span>',
            'forceReload'=>'#tariff-list-pjax',
        ];
    }

    /**
     * Updates an existing Common model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditCommon($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        $tariff = Yii::$app->user->identity->tariff->key;     

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceClose'=>true,
                'forceReload'=>'#common-pjax',
            ];    
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/additional/common/update', [
                    'model' => $model,
                    'tariff' => $tariff,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    // Изменить “Юридические информации”.
    public function actionEditLegal($user_id)
    {
        $request = Yii::$app->request;
        $model = LegalInformation::find()->where(['user_id' => $user_id])->one();
        if($model == null) { $model = new LegalInformation(); $model->user_id = $user_id; }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceClose'=>true,
                'forceReload'=>'#products-pjax',
            ];    
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'normal',
                'content'=>$this->renderAjax('/additional/common/_legal_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    // Изменить “Цены тарифа”.
    public function actionEditTariff($id)
    {
        $request = Yii::$app->request;
        $model = Tariffs::findOne($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $titles = $request->post()['titles'];
                $result = [];
                foreach ($titles as $key => $value) {
                    if($request->post()['prices'][$key] == null) $price = 0;
                    else $price = $request->post()['prices'][$key];
                    $result [] = [
                        'time' => $value,
                        'price' => $price,
                    ];
                }

                $model->price_list = json_encode($result, true);
                $model->save();

                return [
                    'forceClose'=>true,
                    'forceReload'=>'#tariff-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/additional/common/_tariffs_form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    //Избранную пользователем тарифа писать сессию.
    public function actionSetSession($key, $index)
    {
        $tariff = Tariffs::find()->where(['key' => $key])->one();
        $data = json_decode($tariff->price_list,true);
        $price = $data[$index]['price'];
        $time = $data[$index]['time'];

        $session = Yii::$app->session;
        $session['prices'] = [
            'key' => $key,
            'price' => $price,
            'time' => $time,
            'index' => $index,
        ];
    }

    // От пользователей сайта отправка заявок (Обратная связь)  на “Administrator”.
    public function actionSendQuestion()
    {
        $request = Yii::$app->request;
        $model = new Write();       

        if ($model->load($request->post()) && $model->save()) {
            $admin = Users::find()->where(['type' => [0,1] ])->one();
            if($admin != null) {
                Common::sendMessageToEmail(
                    $admin->login, 
                    $model->text, 
                    'Вам отправлено от '.$model->sender_email.' заявка'
                );                
            }
            return $this->render('/additional/common/successful_form', []);
        }else{
            return $this->render('/additional/common/_write_form', [
                'model' => $model,
            ]);
        }
    }

    //Изменение Юридическую информацию к каждому пользователю принадлежащим им.
    public function actionLegalInformation($user_id)
    {
        $request = Yii::$app->request;
        $model = LegalInformation::find()->where([ 'user_id' => $user_id])->one();   

        return $this->render('/additional/common/legal-information', [
            'model' => $model,
        ]);
    }

    // Страница  Договор оферта
    public function actionLegal()
    {
        $legal = Legal::findOne(1);
        $privacy = Legal::getPrivacy();
        $offer = Legal::getOffer();

        return $this->render('/additional/common/legal', [
            'legal' => $legal,
            'privacy' => $privacy,
            'offer' => $offer,
        ]);
    }

    // Страница Блог
    public function actionBlog()
    { 
        $dostup = true;
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;

        if(Yii::$app->user->identity->id == null) $dostup = false;
        if($type == 2) {
            $permission = Permissions::find()->where(['user_id' => $user_id])->one();
            if(!$permission->change_content_page) $dostup = false;
        }
        if($type == 3) {
            $dostup = false;
        }
        $blogs = Blogs::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('/additional/common/blog', [
            'blogs' => $blogs,
            'dostup' => $dostup,
        ]);
    }

    //Добавление нового блога
    public function actionAddBlog()
    {
        $request = Yii::$app->request;
        $model = new Blogs();

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            $model->file = UploadedFile::getInstance($model, 'file');
            if(!empty($model->file)) {
                $model->file->saveAs('images/blog/'.$model->id.'.'.$model->file->extension);
                Yii::$app->db->createCommand()
                    ->update(
                        'blogs', 
                        ['image' => $model->id.'.'.$model->file->extension], 
                        [ 'id' => $model->id ])
                    ->execute();
            }

            return [
                'forceClose'=>true,
                'forceReload'=>'#blog-pjax',
            ];    
        }else{
             return [
                'title'=> "Добавить новый блог",
                'size' => 'large',
                'content'=>$this->renderAjax('/additional/common/blog_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    // Просмотр блога
    public function actionViewBlog($id)
    {
        $request = Yii::$app->request;

        $dostup = true;
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;

        if(Yii::$app->user->identity->id == null) $dostup = false;
        if($type == 2) {
            $permission = Permissions::find()->where(['user_id' => $user_id])->one();
            if(!$permission->change_content_page) $dostup = false;
        }
        if($type == 3) {
            $dostup = false;
        }
        $model = BLogs::findOne($id);

        return $this->render('/additional/common/view-blog', [
            'model' => $model,
            'dostup' => $dostup,
        ]);
    }

    // Изменить блог.
    public function actionEditBlog($id)
    {
        $request = Yii::$app->request;
        $model = Blogs::findOne($id);
        $old = Blogs::findOne($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            $model->file = UploadedFile::getInstance($model, 'file');
            if(!empty($model->file)) {
                if (file_exists('images/blog/'. $old->image))  {
                    unlink(Yii::getAlias('images/blog/' .  $old->image));
                }
                $model->file->saveAs('images/blog/'.$model->id.'.'.$model->file->extension);
                Yii::$app->db->createCommand()->update(
                    'blogs', 
                    ['image' => $model->id.'.'.$model->file->extension], 
                    [ 'id' => $model->id ])
                ->execute();
            }

            return [
                'forceClose'=>true,
                'forceReload'=>'#blog-pjax',
            ];    
        }else{
             return [
                'title'=> "Изменить блог",
                'size' => 'large',
                'content'=>$this->renderAjax('/additional/common/blog_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    // Активирование промо код.
    public function actionGetPromoCode()
    {
        $request = Yii::$app->request;

        if($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()) {

                $code = $request->post()['PromoCodes']['promo-code'];
                $promoCode = PromoCodes::find()
                    ->where(['code' => $code, 'status' => 1])
                    ->andWhere(['>', 'access_date', date('Y-m-d'),])
                    ->andWhere(['>', 'remaining_input', 0])
                    ->one();

                if($promoCode) {
                    $promoCode->remaining_input -= 1;
                    $promoCode->ended_input += 1;
                    $promoCode->save();

                    return [
                        'forceReload'=>'#tariff-list-pjax',
                        'title'=> "Промокод",
                        'content'=>'<center><span style="font-size:18px;font-weight:bold;color:#348fe2;">Успешно выполнено</span></center>',
                    ];
                }else{
                   return [
                        'forceReload'=>'#tariff-list-pjax',
                        'title'=> "Ошибка",
                        'content'=>'<center><span style="font-size:18px;font-weight:bold;color:#ff5b57;">Нет такого промокод</span</center>',
                    ]; 
                }
            }else{
                return [
                    'title'=> "Промокод",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/additional/common/_promo_code_form', [
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
        
    }

    // Удалить блог
    public function actionDeleteBlog($id)
    {
        $request = Yii::$app->request;
        Blogs::findOne($id)->delete();
        return $this->redirect(['/common/blog']);
    }

    /**
     * Finds the Common model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Common the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Common::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
