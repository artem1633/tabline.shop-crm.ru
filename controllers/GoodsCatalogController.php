<?php

namespace app\controllers;

use Yii;
use app\models\GoodsCatalog;
use app\models\GoodsCatalogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Products;

/**
 * GoodsCatalogController implements the CRUD actions for GoodsCatalog model.
 */
class GoodsCatalogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new GoodsCatalog model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new GoodsCatalog();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                foreach ($request->post()['GoodsCatalog']['products'] as $value) {
                    $product = Products::findOne($value);
                    $product->catalog_id = $model->id;
                    $product->save();
                }
                return [
                    'forceReload'=>'#catalog-pjax',
                    'size' => 'normal',
                    'title'=> "Каталог",
                    'content'=>'<span class="text-success">Успешно выпольнено</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['/goods-catalog/create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }       
    }

    /**
     * Updates an existing GoodsCatalog model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $products = Products::find()->where(['catalog_id' => $id])->all();
        $array = [];
        foreach ($products as $value) $array [] = $value->id;
        $model->products = $array;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                
                $products = Products::find()->where([ 'catalog_id' => $model->id ])->all();
                foreach ($products as $value) {
                    $value->catalog_id = null;
                    $value->save();
                }

                foreach ($request->post()['GoodsCatalog']['products'] as $value) {
                    $product = Products::findOne($value);
                    $product->catalog_id = $model->id;
                    $product->save();
                }

                return [
                    'forceReload'=>'#catalog-pjax',
                    'size' => 'normal',
                    'title'=> "Каталог",
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Каталог",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing GoodsCatalog model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
            $product = Products::find()->where(['catalog_id'=> $id])->one();             
            if( $product != null) {
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center>
                                        <span class="text-danger" style="color:red; font-size:20px;">
                                            <b>Вы не можете удалить этот каталог. Потому что он связан с товарами!!!</b>
                                        </span>
                                </center>',                              
                ];
            }else{
                $this->findModel($id)->delete();
                return ['forceClose'=>true,'forceReload'=>'#catalog-pjax'];              
            }
        }
    }

    /**
     * Finds the GoodsCatalog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GoodsCatalog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GoodsCatalog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
