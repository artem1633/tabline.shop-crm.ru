<?php

namespace app\controllers;

use Yii;
use app\models\blocks\ContactFormBlock;
use app\models\blocks\ContactFormBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;
use app\models\UsersPage;

/**
 * ContactFormBlockController implements the CRUD actions for ContactFormBlock model.
 */
class ContactFormBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new ContactFormBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new ContactFormBlock(); 
        $model->setDefaultValues($page_id);
        $model->page_id = $page_id;

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()) {
                $post = $request->post();
                $list_datas = '';
                $select_datas = '';
                if($post['ContactFormBlock']['list_datas'] != null){
                    $i = 0;
                    foreach ($post['ContactFormBlock']['list_datas'] as $value) {
                        if($i == 1) $list_datas .= ',' . $value ;
                        else $list_datas .= $value ;
                        $i = 1;
                    }
                }
                if($post['ContactFormBlock']['select_datas'] != null){
                    $i = 0;
                    foreach ($post['ContactFormBlock']['select_datas'] as $value) {
                        if($i == 1) $select_datas .= ',' . $value;
                        else $list_datas .= $value ;
                        $i = 1;
                    }
                }

                $model->list_data = $list_datas;
                $model->select_data = $select_datas;
                $model->save();
                $page_block = new PageBlocks();
                $page_block->page_id = $page_id;
                $page_block->table_name = 'contact_form_block';
                $page_block->sorting = UsersPage::getSortNumber($page_id);
                $page_block->field_id = $model->id;
                $page_block->save();
                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Добавить форма контактов",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/contact-form-block/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->validate()) {

                $post = $request->post();
                $list_datas = '';
                $i = 0;
                foreach ($post['ContactFormBlock']['list_datas'] as $value) {
                    if($i == 1) $list_datas .= ',' . $value ;
                    else $list_datas .= $value ;
                    $i = 1;
                }
                $select_datas = '';
                $i = 0;
                foreach ($post['ContactFormBlock']['select_datas'] as $value) {
                    if($i == 1) $select_datas .= ',' . $value;
                    else $list_datas .= $value ;
                    $i = 1;
                }

                $model->list_data = $list_datas;
                $model->select_data = $select_datas;
                $model->save();
                $page_block = new PageBlocks();
                $page_block->page_id = $page_id;
                $page_block->table_name = 'contact_form_block';
                $page_block->sorting = UsersPage::getSortNumber($page_id);
                $page_block->field_id = $model->id;
                $page_block->save();

                return $this->redirect([
                    '/users-page/page', 
                    'user_id' => UsersPage::findOne($page_id)->user_id 
                ]);
            } else {
                return $this->render('/blocks/contact-form-block/create', [
                    'model' => $model,
                ]);
            }
        }       
    }

    /**
     * Updates an existing ContactFormBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $post = $request->post();
                $list_datas = '';
                $i = 0;
                $model->list_data = '';
                foreach ($post['ContactFormBlock']['list_datas'] as $list) {
                    if($i == 1) $list_datas .= ',' . $list ;
                    else $list_datas = $list ;
                    $i = 1;
                }
                $select_datas = '';
                $i = 0;
                $model->select_data = '';
                foreach ($post['ContactFormBlock']['select_datas'] as $select) {
                    if($i == 1) $select_datas .= ',' . $select;
                    else $select_datas = $select ;
                    $i=1;
                }
                $model->list_data = $list_datas;
                $model->select_data = $select_datas;
                $model->save();

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/contact-form-block/update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if($model->load($request->post()) && $model->validate()){

                $post = $request->post();
                $list_datas = '';
                $i = 0;
                $model->list_data = '';
                foreach ($post['ContactFormBlock']['list_datas'] as $list) {
                    if($i == 1) $list_datas .= ',' . $list ;
                    else $list_datas = $list ;
                    $i = 1;
                }
                $select_datas = '';
                $i = 0;
                $model->select_data = '';
                foreach ($post['ContactFormBlock']['select_datas'] as $select) {
                    if($i == 1) $select_datas .= ',' . $select;
                    else $select_datas = $select ;
                    $i = 1;
                }
                $model->list_data = $list_datas;
                $model->select_data = $select_datas;
                $model->save();

                return $this->redirect(['/users-page/page', 'user_id' => $model->page->user_id]);  
            }else{
                return $this->render('/blocks/contact-form-block/update', [
                    'model' => $model,
                ]);
            }

        }
    }

    /**
     * Delete an existing ContactFormBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'contact_form_block', 'field_id' => $id ])->one();
        if($pageBlock != null) {
            $pageBlock->delete();
        }
        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }
    /**
     * Finds the ContactFormBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactFormBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactFormBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
