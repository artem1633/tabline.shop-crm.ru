<?php

namespace app\controllers;

use Yii;
use app\models\blocks\AdvantageBlock;
use app\models\blocks\AdvantageBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;
use app\models\Directory;
use app\models\UsersPage;

/**
 * AdvantageBlockController implements the CRUD actions for AdvantageBlock model.
 */
class AdvantageBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new AdvantageBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new AdvantageBlock();
        $model->visible = 1;
        $model->visible_in_menu = 1;
        $model->name_in_menu = 'Преимущество';

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){
                                
                $post = $request->post();
                $number = AdvantageBlock::getLastId();
                $targetDir = Directory::createAdvantageDirectory($number);

                $i=-1;
                $proverka = 0;
                foreach ($_FILES["images"]["tmp_name"] as $value) {
                    $i++;
                    $fileName = $_FILES["images"]["name"][$i];
                    $targetFilePath = $targetDir . $fileName;

                    $status = move_uploaded_file($value, $targetFilePath);

                    $advantage = new AdvantageBlock();
                    $advantage->page_id = $page_id;
                    $advantage->number = $number;
                    $advantage->image = $fileName;
                    $advantage->title = $post['titles'][$i];
                    $advantage->text = $post['texts'][$i];
                    $advantage->visible = $post['AdvantageBlock']['visible'];
                    $advantage->visible_in_menu = $post['AdvantageBlock']['visible_in_menu'];
                    $advantage->title_text = $post['AdvantageBlock']['title_text'];
                    $advantage->name_in_menu = $post['AdvantageBlock']['name_in_menu'];
                    if($advantage->save()) $proverka = 1;
                }

                if($proverka) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'advantage_block';
                    $page_block->sorting = UsersPage::getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Преимущество",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/advantage-block/create', [ 'model' => $model, ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($request->post()) {

                $post = $request->post();
                $number = AdvantageBlock::getLastId();
                $targetDir = Directory::createAdvantageDirectory($number);

                $i=-1;
                $proverka = 0;
                foreach ($_FILES["images"]["tmp_name"] as $value) {
                    $i++;
                    $fileName = $_FILES["images"]["name"][$i];
                    $targetFilePath = $targetDir . $fileName;

                    $status = move_uploaded_file($value, $targetFilePath);

                    $advantage = new AdvantageBlock();
                    $advantage->page_id = $page_id;
                    $advantage->number = $number;
                    $advantage->image = $fileName;
                    $advantage->title = $post['titles'][$i];
                    $advantage->text = $post['texts'][$i];
                    $advantage->visible = $post['AdvantageBlock']['visible'];
                    $advantage->visible_in_menu = $post['AdvantageBlock']['visible_in_menu'];
                    $advantage->title_text = $post['AdvantageBlock']['title_text'];
                    $advantage->name_in_menu = $post['AdvantageBlock']['name_in_menu'];
                    if($advantage->save()) $proverka = 1;
                }

                if($proverka) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'advantage_block';
                    $page_block->sorting = UsersPage::getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return $this->redirect(['/users-page/page', 'user_id' => UsersPage::findOne($page_id)->user_id ]);
            } else {
                return $this->render('/blocks/advantage-block/create', [ 'model' => $model, ]);
            }
        }       
    }

    /**
     * Updates an existing AdvantageBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($number, $page_id)
    {
        $request = Yii::$app->request;
        $advantages = AdvantageBlock::find()->where(['number' => $number])->all();
        $count = AdvantageBlock::find()->where(['number' => $number])->count();
        $model = AdvantageBlock::find()->where(['number' => $number])->one();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if( $request->post() ){

                $post = $request->post();                
                //удаление слайды из бд
                foreach ($advantages as $advantage) {
                    $isset = 0;
                    foreach ($post['advantage_id'] as $key => $value) {
                        if( $value == $advantage->id ) $isset = 1;
                    }
                    if($isset == 0) {
                        if (file_exists('images/advantage-block/' . $number . '/' . $advantage->image)) {
                            unlink(Yii::getAlias('images/advantage-block/' . $number . '/' . $advantage->image));
                        } 
                        $advantage->delete();
                    }
                }

                $max_number = -1;
                $targetDir = 'images/advantage-block/' . $number . '/';
                foreach ($post['advantage_id'] as $key => $value) {

                    $max_number = $key;
                    $advantage = AdvantageBlock::findOne($value);
                    if($advantage != null) {      

                        $advantage->title = $post['titles'][$key];
                        $advantage->text = $post['texts'][$key];
                        $advantage->visible = $post['AdvantageBlock']['visible'];
                        $advantage->visible_in_menu = $post['AdvantageBlock']['visible_in_menu'];
                        $advantage->title_text = $post['AdvantageBlock']['title_text'];
                        $advantage->name_in_menu = $post['AdvantageBlock']['name_in_menu'];

                        if($_FILES["images"]["tmp_name"][$key] != null || $_FILES["images"]["tmp_name"][$key] == '') {
                            $fileName = $_FILES["images"]["name"][$key];
                            $targetFilePath = $targetDir . $fileName;

                            if(move_uploaded_file($_FILES["images"]["tmp_name"][$key], $targetFilePath)) 
                            {
                                if (file_exists('images/advantage-block/' . $number . '/' . $advantage->image))  {
                                    unlink(Yii::getAlias('images/advantage-block/' . $number . '/' . $advantage->image));
                                }
                                $advantage->image = $fileName;
                            }
                        }

                        $advantage->save();
                    }
                }

                $i = -1;
                if(AdvantageBlock::find()->where(['number' => $number])->one() == null) $max_number = -1;
                foreach ($_FILES["images"]["tmp_name"] as $key => $value) {
                    $i++;
                    if($key > $max_number) {

                        $fileName = $_FILES["images"]["name"][$i];
                        $targetFilePath = $targetDir . $fileName;
                        if(move_uploaded_file($value, $targetFilePath)) {
                            $fileName = $_FILES["images"]["name"][$i];
                        }

                        $advantage = new AdvantageBlock();
                        $advantage->page_id = $page_id;
                        $advantage->number = $number;                        
                        $advantage->image = $fileName;
                        $advantage->title = $post['titles'][$i];
                        $advantage->text = $post['texts'][$i];
                        $advantage->visible = $post['AdvantageBlock']['visible'];
                        $advantage->visible_in_menu = $post['AdvantageBlock']['visible_in_menu'];
                        $advantage->title_text = $post['AdvantageBlock']['title_text'];
                        $advantage->name_in_menu = $post['AdvantageBlock']['name_in_menu'];
                        $advantage->save();
                    }
                }
                
                if(AdvantageBlock::find()->where(['number' => $number])->one() == null) {
                    PageBlocks::find()->where(['table_name' => 'advantage_block', 'field_id' => $number ])->one()->delete();
                }                

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/advantage-block/update', [
                        'advantages' => $advantages,
                        'number' => $number,
                        'count' => $count,
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($request->post()) {

                $post = $request->post();                
                //удаление слайды из бд
                foreach ($advantages as $advantage) {
                    $isset = 0;
                    foreach ($post['advantage_id'] as $key => $value) {
                        if( $value == $advantage->id ) $isset = 1;
                    }
                    if($isset == 0) {
                        if (file_exists('images/advantage-block/' . $number . '/' . $advantage->image))  {
                            unlink(Yii::getAlias('images/advantage-block/' . $number . '/' . $advantage->image));
                        }
                        $advantage->delete();
                    }
                }

                $max_number = -1;
                $targetDir = 'images/advantage-block/' . $number . '/';
                foreach ($post['advantage_id'] as $key => $value) {
                    $max_number = $key;
                    $advantage = AdvantageBlock::findOne($value);
                    if($advantage != null) {                        
                        $advantage->title = $post['titles'][$key];
                        $advantage->text = $post['texts'][$key];
                        $advantage->visible = $post['AdvantageBlock']['visible'];
                        $advantage->visible_in_menu = $post['AdvantageBlock']['visible_in_menu'];
                        $advantage->title_text = $post['AdvantageBlock']['title_text'];
                        $advantage->name_in_menu = $post['AdvantageBlock']['name_in_menu'];

                        if($_FILES["images"]["tmp_name"][$key] != null || $_FILES["images"]["tmp_name"][$key] == '') {
                            $fileName = $_FILES["images"]["name"][$key];
                            $targetFilePath = $targetDir . $fileName;

                            if(move_uploaded_file($_FILES["images"]["tmp_name"][$key], $targetFilePath)) {
                                if (file_exists('images/advantage-block/' . $number . '/' . $advantage->image)) {
                                    unlink(Yii::getAlias('images/advantage-block/' . $number . '/' . $advantage->image));
                                }
                                $advantage->image = $fileName;
                            }
                        }

                        $advantage->save();
                    }
                }

                $i = -1;
                if(AdvantageBlock::find()->where(['number' => $number])->one() == null) $max_number = -1;
                foreach ($_FILES["images"]["tmp_name"] as $key => $value) {
                    $i++;
                    if($key > $max_number) {

                        $fileName = $_FILES["images"]["name"][$i];
                        $targetFilePath = $targetDir . $fileName;
                        if(move_uploaded_file($value, $targetFilePath)) {
                            $fileName = $_FILES["images"]["name"][$i];
                        }

                        $advantage = new AdvantageBlock();
                        $advantage->page_id = $page_id;
                        $advantage->number = $number;                        
                        $advantage->image = $fileName;
                        $advantage->title = $post['titles'][$i];
                        $advantage->text = $post['texts'][$i];
                        $advantage->visible = $post['AdvantageBlock']['visible'];
                        $advantage->visible_in_menu = $post['AdvantageBlock']['visible_in_menu'];
                        $advantage->title_text = $post['AdvantageBlock']['title_text'];
                        $advantage->name_in_menu = $post['AdvantageBlock']['name_in_menu'];
                        $advantage->save();
                    }
                }
                
                if(AdvantageBlock::find()->where(['number' => $number])->one() == null) {
                    PageBlocks::find()->where(['table_name' => 'advantage_block', 'field_id' => $number ])->one()->delete();
                }

                return $this->redirect(['/users-page/page', 'user_id' => UsersPage::findOne($page_id)->user_id ]);
            } else {
                return $this->render('/blocks/advantage-block/update', [
                    'advantages' => $advantages,
                    'number' => $number,
                    'count' => $count,
                    'model' => $model,
                ]);
            }
        }       
    }

    /**
     * Remove an existing AdvantageBlock model.
     * For ajax request will return json object
     * @param integer $number
     * @return mixed
     */
    public function actionRemove($number)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'advantage_block', 'field_id' => $number ])->one();
        if($pageBlock != null) {
            $pageBlock->delete();
        }
        $advantage = AdvantageBlock::find()->where(['number' => $number])->all();
        foreach ($advantage as $value) {
            $value->delete();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the AdvantageBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdvantageBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdvantageBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
