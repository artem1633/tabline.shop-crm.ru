<?php

namespace app\controllers;

use Yii;
use app\models\blocks\CaruselBlock;
use app\models\blocks\CaruselBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;
use app\models\Directory;
use app\models\UsersPage;
use app\models\additional\Clicks;
/**
 * CaruselBlockController implements the CRUD actions for CaruselBlock model.
 */
class CaruselBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new CaruselBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new CaruselBlock();
        $model->visible = 1;
        $model->visible_in_menu = 1;
        $model->name_in_menu = 'Карусель';

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){
                                
                $post = $request->post();
                $model->load($request->post());
                $number = CaruselBlock::getLastId();
                $targetDir = Directory::createCaruselDirectory($number);

                $i=-1;
                $all_images = [];
                $proverka = 0;
                foreach ($_FILES["images"]["tmp_name"] as $value) {
                    $i++;
                    $fileName = $_FILES["images"]["name"][$i];
                    $targetFilePath = $targetDir . $fileName;

                    if(move_uploaded_file($value, $targetFilePath)) $all_images [] = $fileName;
                    else $all_images [] = null;

                    $carusel = new CaruselBlock();
                    $carusel->page_id = $page_id;
                    $carusel->number = $number;
                    $carusel->size_slide = $model->size_slide;
                    $carusel->time = $model->time;
                    $carusel->image = $fileName;
                    $carusel->title = $post['titles'][$i];
                    $carusel->text = $post['texts'][$i];
                    $carusel->visible = $post['CaruselBlock']['visible'];
                    $carusel->visible_in_menu = $post['CaruselBlock']['visible_in_menu'];
                    $carusel->title_text = $post['CaruselBlock']['title_text'];
                    $carusel->name_in_menu = $post['CaruselBlock']['name_in_menu'];
                    if($carusel->save()) $proverka = 1;
                }
                if($proverka) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'carusel_block';
                    $page_block->sorting = UsersPage::getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Карусель картинок",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/carusel-block/create', [
                        'model' => $model,
                        'day_count' => 0,
                        'week_count' => 0,
                        'month_count' => 0,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($request->post()) {

                $post = $request->post();
                $model->load($request->post());
                $number = CaruselBlock::getLastId();
                $targetDir = Directory::createCaruselDirectory($number);

                $i=-1;
                $all_images = [];
                $proverka = 0;
                foreach ($_FILES["images"]["tmp_name"] as $value) {
                    $i++;
                    $fileName = $_FILES["images"]["name"][$i];
                    $targetFilePath = $targetDir . $fileName;

                    if(move_uploaded_file($value, $targetFilePath)) $all_images [] = $fileName;
                    else $all_images [] = null;

                    $carusel = new CaruselBlock();
                    $carusel->page_id = $page_id;
                    $carusel->number = $number;
                    $carusel->size_slide = $model->size_slide;
                    $carusel->time = $model->time;
                    $carusel->image = $fileName;
                    $carusel->title = $post['titles'][$i];
                    $carusel->text = $post['texts'][$i];
                    $carusel->visible = $post['CaruselBlock']['visible'];
                    $carusel->visible_in_menu = $post['CaruselBlock']['visible_in_menu'];
                    $carusel->title_text = $post['CaruselBlock']['title_text'];
                    $carusel->name_in_menu = $post['CaruselBlock']['name_in_menu'];
                    if($carusel->save()) $proverka = 1;
                }
                if($proverka) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'carusel_block';
                    $page_block->sorting = UsersPage::getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return $this->redirect(['/users-page/page', 'user_id' => UsersPage::findOne($page_id)->user_id ]);
            }else{
                return $this->render('/blocks/carusel-block/create', [
                    'model' => $model,
                    'day_count' => 0,
                    'week_count' => 0,
                    'month_count' => 0,
                ]);
            }
        }       
    }

    /**
     * Updates an existing CaruselBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($number, $page_id)
    {
        $request = Yii::$app->request;
        $carusels = CaruselBlock::find()->where(['number' => $number])->all();
        $count = CaruselBlock::find()->where(['number' => $number])->count();
        $model = CaruselBlock::find()->where(['number' => $number])->one();

        $week = date('Y-m-d', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) );

        $day_count = Clicks::find()
            ->where([
                'table_name' => 'carusel_block', 
                'field_id' => $number 
            ])
            ->andWhere(['date' => date('Y-m-d')])
            ->count(); 

        $week_count = Clicks::find()
            ->where([
                'table_name' => 'carusel_block', 
                'field_id' => $number 
            ])
            ->andWhere(['between', 'date', $week, date('Y-m-d') ])
            ->count(); 

        $month_count = Clicks::find()
            ->where([
                'table_name' => 'carusel_block', 
                'field_id' => $number 
            ])
            ->andWhere(['between', 'date', date('Y-m-01'), date('Y-m-d') ])
            ->count();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if( $request->post() ){

                $post = $request->post();
                $model->load($request->post());
                //удаление слайды из бд
                foreach ($carusels as $carusel) {
                    $isset = 0;
                    foreach ($post['carusel_id'] as $key => $value) {
                        if( $value == $carusel->id ) $isset = 1;
                    }
                    if($isset == 0) {
                        if (file_exists('images/carusel-block/' . $number . '/' . $carusel->image))  {
                            unlink(Yii::getAlias('images/carusel-block/' . $number . '/' . $carusel->image));
                        }
                        $carusel->delete();
                    }
                }

                $max_number = -1;
                $targetDir = 'images/carusel-block/' . $number . '/';
                foreach ($post['carusel_id'] as $key => $value) {
                    $max_number = $key;
                    $carusel = CaruselBlock::findOne($value);
                    if($carusel != null) {
                        $carusel->size_slide = $model->size_slide;
                        $carusel->time = $model->time;
                        $carusel->title = $post['titles'][$key];
                        $carusel->text = $post['texts'][$key];
                        $carusel->visible = $post['CaruselBlock']['visible'];
                        $carusel->visible_in_menu = $post['CaruselBlock']['visible_in_menu'];
                        $carusel->title_text = $post['CaruselBlock']['title_text'];
                        $carusel->name_in_menu = $post['CaruselBlock']['name_in_menu'];

                        if($_FILES["images"]["tmp_name"][$key] != null || $_FILES["images"]["tmp_name"][$key] == '') {
                            $fileName = $_FILES["images"]["name"][$key];
                            $targetFilePath = $targetDir . $fileName;

                            if(move_uploaded_file($_FILES["images"]["tmp_name"][$key], $targetFilePath)) {
                                if (file_exists('images/carusel-block/' . $number . '/' . $carusel->image)) {
                                    unlink(Yii::getAlias('images/carusel-block/' . $number . '/' . $carusel->image));
                                }
                                $carusel->image = $fileName;
                            }
                        }

                        $carusel->save();
                    }
                }

                $i = -1;
                if(CaruselBlock::find()->where(['number' => $number])->one() == null) $max_number = -1;
                foreach ($_FILES["images"]["tmp_name"] as $key => $value) {
                    $i++;
                    if($key > $max_number) {

                        $fileName = $_FILES["images"]["name"][$i];
                        $targetFilePath = $targetDir . $fileName;
                        if(move_uploaded_file($value, $targetFilePath)) $fileName = $_FILES["images"]["name"][$i];

                        $carusel = new CaruselBlock();
                        $carusel->page_id = $page_id;
                        $carusel->number = $number;
                        $carusel->size_slide = $model->size_slide;
                        $carusel->time = $model->time;
                        $carusel->image = $fileName;
                        $carusel->title = $post['titles'][$i];
                        $carusel->text = $post['texts'][$i];
                        $carusel->visible = $post['CaruselBlock']['visible'];
                        $carusel->visible_in_menu = $post['CaruselBlock']['visible_in_menu'];
                        $carusel->title_text = $post['CaruselBlock']['title_text'];
                        $carusel->name_in_menu = $post['CaruselBlock']['name_in_menu'];
                        $carusel->save();
                    }
                }
                
                if(CaruselBlock::find()->where(['number' => $number])->one() == null) {
                    PageBlocks::find()->where(['table_name' => 'carusel_block', 'field_id' => $number ])->one()->delete();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/carusel-block/update', [
                        'carusels' => $carusels,
                        'number' => $number,
                        'count' => $count,
                        'model' => $model,
                        'day_count' => $day_count,
                        'week_count' => $week_count,
                        'month_count' => $month_count,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($request->post()) {

                $post = $request->post();
                $model->load($request->post());
                //удаление слайды из бд
                foreach ($carusels as $carusel) {
                    $isset = 0;
                    foreach ($post['carusel_id'] as $key => $value) {
                        if( $value == $carusel->id ) $isset = 1;
                    }
                    if($isset == 0) {
                        if (file_exists('images/carusel-block/' . $number . '/' . $carusel->image))  {
                            unlink(Yii::getAlias('images/carusel-block/' . $number . '/' . $carusel->image));
                        }
                        $carusel->delete();
                    }
                }

                $max_number = -1;
                $targetDir = 'images/carusel-block/' . $number . '/';
                foreach ($post['carusel_id'] as $key => $value) {
                    $max_number = $key;
                    $carusel = CaruselBlock::findOne($value);
                    if($carusel != null){
                        $carusel->size_slide = $model->size_slide;
                        $carusel->time = $model->time;
                        $carusel->title = $post['titles'][$key];
                        $carusel->text = $post['texts'][$key];

                        if($_FILES["images"]["tmp_name"][$key] != null || $_FILES["images"]["tmp_name"][$key] == '') {
                            $fileName = $_FILES["images"]["name"][$key];
                            $targetFilePath = $targetDir . $fileName;

                            if(move_uploaded_file($_FILES["images"]["tmp_name"][$key], $targetFilePath)) {
                                if (file_exists('images/carusel-block/' . $number . '/' . $carusel->image))  {
                                    unlink(Yii::getAlias('images/carusel-block/' . $number . '/' . $carusel->image));
                                }
                                $carusel->image = $fileName;
                            }
                        }

                        $carusel->save();
                    }
                }

                $i = -1;
                if(CaruselBlock::find()->where(['number' => $number])->one() == null) {
                    $max_number = -1;
                }
                foreach ($_FILES["images"]["tmp_name"] as $key => $value) {
                    $i++;
                    if($key > $max_number) {

                        $fileName = $_FILES["images"]["name"][$i];
                        $targetFilePath = $targetDir . $fileName;
                        if(move_uploaded_file($value, $targetFilePath)) {
                            $fileName = $_FILES["images"]["name"][$i];
                        }

                        $carusel = new CaruselBlock();
                        $carusel->page_id = $page_id;
                        $carusel->number = $number;
                        $carusel->size_slide = $model->size_slide;
                        $carusel->time = $model->time;
                        $carusel->image = $fileName;
                        $carusel->title = $post['titles'][$i];
                        $carusel->text = $post['texts'][$i];
                        $carusel->save();
                    }
                }
                
                if(CaruselBlock::find()->where(['number' => $number])->one() == null) {
                    PageBlocks::find()->where(['table_name' => 'carusel_block', 'field_id' => $number ])->one()->delete();
                }

                return $this->redirect(['/users-page/page', 'user_id' => UsersPage::findOne($page_id)->user_id ]);
            } else {
                return $this->render('/blocks/carusel-block/update', [
                    'carusels' => $carusels,
                    'number' => $number,
                    'count' => $count,
                    'model' => $model,
                    'day_count' => $day_count,
                    'week_count' => $week_count,
                    'month_count' => $month_count,
                ]);
            }
        }       
    }

    /**
     * Remove an existing CaruselBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $number
     * @return mixed
     */
    public function actionRemove($number)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'carusel_block', 'field_id' => $number ])->one();
        if($pageBlock != null) {
            $pageBlock->delete();
        }
        $carusel = CaruselBlock::find()->where(['number' => $number])->all();
        foreach ($carusel as $value) {
            $value->delete();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the CaruselBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CaruselBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CaruselBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
