<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use MetzWeb\Instagram\Instagram;
use app\models\Settings;
use app\models\User;
use app\models\additional\Permissions;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 3) {
            if($action->id != 'view' && $action->id != 'list' && $action->id != 'change') {
                return $this->redirect(['/site/errors', 'type' => 0 ]);
            }
        }
        if($type == 2) {
            $permission = Permissions::find()->where(['user_id' => $user_id])->one();
            if(!$permission->view_user && $action->id == 'index') {
                return $this->redirect(['/site/errors', 'type' => 1 ]);
            }
            if(!$permission->delete_user && $action->id == 'delete') {
                return $this->redirect(['/site/errors', 'type' => 2 ]);
            }
            if(!$permission->create_user && $action->id == 'create') {
                return $this->redirect(['/site/errors', 'type' => 2 ]);
            }
        }
        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Пользователь #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewInTable($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ФИО : " . $this->findModel($id)->fio,
                    'content'=>$this->renderAjax('view_in_table', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view_in_table', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                $model->relation = $model->id;
                $model->save();
                $model->file = UploadedFile::getInstance($model, 'file');

                if(!empty($model->file)) {
                    $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', 
                        [ 'avatar' => $model->id.'.'.$model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Создать",
                    'content'=>'<span class="text-success">Успешно выпольнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post()) && $model->save()) {
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)) {
                    if (file_exists('avatars/'.$model->avatar)) {
                        unlink(Yii::getAlias('avatars/'. $model->avatar));
                    }
                    $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', 
                        [ 'avatar' => $model->id.'.'.$model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Пользователь",
                    'content'=>$this->renderAjax('view_in_table', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> $model->getTypeDescription(),
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)) {
                    
                    if (file_exists('avatars/'.$model->avatar)) {
                        unlink(Yii::getAlias('avatars/'. $model->avatar));
                    }
                    $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', 
                        [ 'avatar' => $model->id.'.'.$model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }
                return [
                    'forceClose' => true,
                    'forceReload'=>'#change-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => "normal",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Lists all connection users.
     * @return mixed
     */
    public function actionList()
    {    
        $searchModel = new UsersSearch();
        $relation = Yii::$app->user->identity->relation;
        $dataProvider = $searchModel->getList(Yii::$app->request->queryParams, $relation);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //Выход из аккаунта.
    public function actionExitFromAccaunt($url)
    {  
        $relation = Yii::$app->user->identity->relation; 
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $session['relation'] = $relation;
        $this->redirect($url);
    }

    //Подключить другого аккаунта.
    public function actionToggle($id)
    {  
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Users::findOne($id);
        Yii::$app->user->logout();
        $new_user = User::findByUsername($user->login);
        Yii::$app->user->login($new_user, 3600*24*30 );

        return [
            //'forceClose'=>true,
            'forceReload'=>'#list-pjax',
            'size' => 'normal',
            'title'=> "Подключенные профили",
            'content'=>'<center><span style="font-size:16px; font-weight:bold;" class="text-success">Успешно выполнено</span></center>',
        ];   
    }

    /**
     * Connection new user to present user.
     * @return mixed
     */
    public function actionAddNewProfile()
    {    
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        /////--------------------------------------Instagram---------------------------------------------------/////
        $instagram = new Instagram(array(
            'apiKey'      => Settings::find()->where(['key' => 'instagram_client_id'])->one()->value,
            'apiSecret'   => Settings::find()->where(['key' => 'instagram_client_secret_key'])->one()->value,
            'apiCallback' => Settings::find()->where(['key' => 'instagram_api_callback'])->one()->value,
        ));
        /////---------------------------------------VK----------------------------------------------------------/////
        $app_id = Settings::find()->where(['key' => 'vk_id_app'])->one()->value;
        $my_url = Settings::find()->where(['key' => 'vk_api_callback'])->one()->value;
        $dialog_url = 'https://oauth.vk.com/authorize?client_id='.$app_id.'&redirect_uri='.$my_url.'&response_type=code&display=page&scope=nohttps,groups,photos,friends,offline';
        /////---------------------------------------FaceBook---------------------------------------------------//////
        $client_id  = '225377498106610';
        $client_secret  = '8e9698070e36c04bc0b73053e1a1add5';
        $redirect_uri  = 'http://rieltor.samar.uz/site/facebook';

        $url = 'https://www.facebook.com/dialog/oauth';

        $params = array(
            'client_id'     => $client_id,
            'redirect_uri'  => $redirect_uri,
            'response_type' => 'code',
            'scope'         => 'email,user_birthday'
        );
        $url_facebook = $url . '?' . urldecode(http_build_query($params));

        return [
            'title'=> "Новый профиль",
            'size' => "normal",
            'content'=>$this->renderAjax('new-profile-form', [
                'instagram' => $instagram,
                'dialog_url' => $dialog_url,
                'url_facebook' => $url_facebook,
            ]),
        ];
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($this->findModel($id)->type != 0) $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
