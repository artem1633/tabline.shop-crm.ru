<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\additional\Common;

$currency = Common::find()->where(['user_id' => $user->id])->one()->getCurrencyFont();

$size = '150';
if (!file_exists('avatars/' . $user->avatar) || $user->avatar == '')  $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
else $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/' . $user->avatar;

if($product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $product->last_price . ' ' . $currency . '</s> ';
else $last = '';

$session = Yii::$app->session;
$count = 0;
foreach ($session['list'] as $key => $value) 
{
    $count += $value['count'];
}

?>
<div class="avatar-block" id="avatar_block" style="width: <?=$size?>px; margin: 0 auto;">
    <img src="<?=$path?>" alt="" style="width: <?=$size?>px; height: <?=$size?>px; border-radius: 50%; margin: 0 auto;">
    <p style="margin: 0; font-size: 13px;text-align: center;"><?=$user->fio?></p>
</div>


<div style="padding-top:50px;">
    <div class="col-md-12 col-xs-12">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php 
                $i = -1;
                $data = json_decode($product->photos, true);
                foreach ($data as $value) {
                ?>
                <li data-target="#myCarousel" data-slide-to="<?=$i?>" <?= $i == 0 ? 'class="active"' : '' ?> ></li>
                <?php } ?>
            </ol>
                            
            <div class="carousel-inner" style="height: auto; max-height: 400px;">
                <?php 
                $i = 0;
                foreach ($data as $value) {
                    $path = "http://". $_SERVER['SERVER_NAME'] . '/';
                    $i++;
                    $path .= $value['path'];
                ?>
                    <div class="item <?= $i == 1 ? 'active' : '' ?> " style=" height: 400px; background-image: url('<?=$path?>');" ></div>
                <?php }
                    if($i == 0){ 
                    $path = "http://". $_SERVER['SERVER_NAME'] . '/images/no-images.jpg';
                ?>
                    <div class="item active " >
                        <center>                                                    
                            <?= Html::a('<img style="height: 100%; width: 100%;" src="'.$path.'">', ['/products/view?id='.$product->id], ['class' => 'product-container-outer']); ?>
                        </center>
                    </div>
                <?php } ?>

            </div>
                            
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <br>
        <div class="table-responsive">
            <table class="table table-bordered">    
                <tr style=" font-size: 16px;font-weight: bold; text-align: center;">
                    <td>Наименование товара</td>
                    <td>Каталог</td>
                    <td>Вес товара</td>
                    <td>Доставка</td>
                    <td>Цена</td>
                </tr>
                <tr style="text-align: center;">
                    <td>
                        <?=$product->name?>
                    </td>
                    <td>
                        <?=$product->catalog->name?>
                    </td>
                    <td>
                        <?=$product->weight?>
                    </td>
                    <td>
                        <?=$product->delivery == 1 ? 'Есть' : 'Нет' ?>
                    </td>
                    <td>
                        <div style=" font-size: 16px; font-weight: bold; color:red; text-align: center;">
                            <?= $last . $product->price . ' ' . $currency;?>
                        </div>
                    </td>
                </tr>
                <tr style="text-align: center;">
                    <td colspan="5">
                        <?=$product->description?>
                   </td>
                </tr>
                <tr style="text-align: center;">
                    <td colspan="5">

                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'cash-pjax']) ?>
                        
                        <?= Html::a('<i class="fa fa-shopping-cart"></i>', ['/products/shop?user_id='.$user->id,], ['class' => 'btn btn-default', 'data-pjax' => '0', 'id' => 'cart  ', 'style' => 'width:100px;', 'title' => 'Магазин']); ?>

                        <a onclick = "alert('Продукт успешно добавлен на корзину!'); $.post('/products/set-product?product_id=<?=$product->id?>&count=1'); $.pjax.reload({container:'#cash-pjax', async: false}); " class="btn btn-warning" style="font-size: 16px; width:250px; font-weight: bold;">Добавить в корзину</a>                        

                        <?= Html::a('<i class="fa fa-shopping-basket"></i><sup style="color:blue; font-weight:bold;">'.$count.'</sup>', ['/shopping-cart/index?user_id='.$user->id,], ['class' => 'btn btn-default', 'id' => 'basket', 'style' => 'width:100px;', 'title' => 'Корзинка']); ?>
                        
                        <?php Pjax::end() ?>
                   </td>
                </tr>
            </table>
        </div>
    </div>
</div>