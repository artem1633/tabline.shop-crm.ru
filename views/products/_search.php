<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<div class="client-form" style="margin-top: 20px;">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-3"> 
                <?= $form->field($model, 'search_name')->textInput(['value' => $post['ProductsSearch']['search_name'] ]) ?>
            </div>

            <div class="col-md-2"> 
                <?= $form->field($model, 'search_price')->textInput([ 'type' => 'number', 'value' => $post['ProductsSearch']['search_price'] ]) ?>
            </div>

            <div class="col-md-3"> 
                <?= $form->field($model, 'search_catalog')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getCatalogList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'value' => $post['ProductsSearch']['search_catalog'] ,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

            <div class="col-md-1">
                <?= Html::submitButton('Поиск' , ['style'=>'margin-top:25px;','class' =>  'btn btn-primary']) ?>
            </div>
            <div class="col-md-1">
                <?= Html::a('<i class="fa fa-cloud-upload"></i> Импорт', ['/products/import'], [ 'style'=> 'margin-top:25px;', 'class' => 'btn btn-default', 'role'=>'modal-remote']); ?>
            </div>
            <div class="col-md-1">
                <?= Html::a('<i class="fa fa-shopping-bag"></i> Магазин', [ '/products/shop', 'user_id' => Yii::$app->user->identity->id ], [ 'style'=> 'margin-top:25px;', 'class' => 'btn btn-default', 'data-pjax' => '0', 'target'=> "_blank" ]); ?>
            </div>
            <div class="col-md-1">
                <?= Html::a('<i class="fa fa-bars fa-rotate-90"></i>', ['/products/sorting',], [ 'title' => 'Настроить колонки таблицы', 'style'=>'margin-top:25px;', 'class' => 'btn btn-default', 'data-pjax'=>0, ]); ?>
            </div>
         </div>   
    <?php ActiveForm::end(); ?>
    
</div>
