<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use app\models\Products;

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>

<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">Активные</span>
                            <span class="hidden-xs">Активные</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Архив</span>
                            <span class="hidden-xs">Архив</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Каталог</span>
                            <span class="hidden-xs">Каталог</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="default-tab-1">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'active-pjax']) ?>
                                <div class="col-md-12 col-sm-12">  
                                    <?= $this->render('_search', ['model' => $searchModel, 'post' => $post]); ?>
                                    <?=GridView::widget([
                                        'id'=>'crud-datatable',
                                        'dataProvider' => $dataProvider,
                                        //'filterModel' => $searchModel,
                                        'pjax'=>true,
                                        'columns' => Products::getColumns(),
                                        //'panelBeforeTemplate' => '',
                                        'toolbar'=> [
                                            ['content'=>
                                                Html::a('Новый товар <i class="glyphicon glyphicon-plus"></i>', ['/products/create'],
                                                ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-success'])
                                            ],
                                        ],    
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,
                                        'panel' => [
                                            'headingOptions' => ['style' => 'display: none;'],
                                            'after'=>'',
                                        ]
                                        ])
                                    ?>                                    
                                </div>

                            <?php Pjax::end() ?>                        
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-2">
                        <div class="row">
                       
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'archive-pjax']) ?>
                                <div class="col-md-12">
                                    <?= $this->render('_search_archive', ['model' => $searchModel, 'post' => $post]); ?>
                                    <?=GridView::widget([
                                        'id'=>'crud-datatable',
                                        'dataProvider' => $archives,
                                        //'filterModel' => $searchArchives,
                                        'pjax'=>true,
                                        'columns' => require(__DIR__.'/_columns.php'),
                                        'panelBeforeTemplate' => '',
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,
                                        'panel' => [
                                            'headingOptions' => ['style' => 'display: none;'],
                                            'after'=>'',
                                        ]
                                        ])
                                    ?>
                                </div>
                            <?php Pjax::end() ?>                 
                        </div>
                    </div>

                    <div class="tab-pane fade" id="default-tab-3">
                        <div class="row">                   
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'catalog-pjax']) ?>
                                <div class="col-md-12">
                                    <?=GridView::widget([
                                        'id'=>'catalog-datatable',
                                        'dataProvider' => $catalogs,
                                        'filterModel' => $catalogSearch,
                                        'pjax'=>true,
                                        'columns' => require(__DIR__.'/_goods_catalog_columns.php'),
                                        'toolbar'=> [
                                            ['content'=>
                                                Html::a('Новый каталог <i class="glyphicon glyphicon-plus"></i>', ['/goods-catalog/create'],
                                                ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-success'])
                                            ],
                                        ],    
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,
                                        'panel' => [
                                            'headingOptions' => ['style' => 'display: none;'],
                                            'after'=>'',
                                        ]
                                        ])
                                    ?>
                                </div>
                            <?php Pjax::end() ?>                 
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
