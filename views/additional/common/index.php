<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use app\models\Products;

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>
<h1>Настройки</h1>
<br>
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="">
                        <a href="#default-tab-1" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Дизайн&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Дизайн&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Общие&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Общие&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Платежи&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Платежи&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#default-tab-4" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Товары&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Товары&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade " id="default-tab-1">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'design-pjax']) ?>
                                <div class="col-md-12 col-sm-12">  
                                    <h3>В работе</h3>                         
                                </div>

                            <?php Pjax::end() ?>                        
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-2">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'common-pjax']) ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                Изменить <a class="btn btn-xs btn-warning" role="modal-remote" title="Изменить" href="<?=Url::toRoute(['edit-common', 'id' => $common->id])?>"><i class="fa fa-pencil"></i></a>
                                            </h3>                                            
                                        </div>
                                        <div class="panel-body">                    
                                            <div class="table-responsive">
                                                <table class="table table-bordered">    
                                                    <tr style="font-size: 16px; font-weight: bold; text-align: center;">
                                                        <td>Часавой пояс</td>
                                                        <td>Валюта</td>
                                                        <td>Facebook Pixel ID</td>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <td style="width:33%;">
                                                            <?=$common->getTimezoneDescription()?>
                                                        </td> 
                                                        <td style="width:33%;">
                                                            <?=$common->getCurrencyDescription()?>
                                                        </td>                                                    
                                                        <td style="width:33%;">
                                                            <?= $common->pixel_id == null ? '<i style="color:red;">(не задано)</i>' : $common->pixel_id ?>
                                                        </td> 
                                                    </tr>
                                                    <tr style="font-size: 16px; font-weight: bold; text-align: center;">
                                                        <td colspan="3">Вставка HTML кода</td>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <td colspan="3">
                                                            <?= $common->html_code == null ? '<i style="color:red;">(не задано)</i>' : $common->html_code ?>
                                                        </td> 
                                                    </tr>                                                    
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php Pjax::end() ?>
                    </div>

                    <div class="tab-pane fade" id="default-tab-3">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'payments-pjax']) ?>
                                <div class="col-md-12">
                                    <h3>В работе</h3>
                                </div>
                            <?php Pjax::end() ?>
                        </div>
                    </div>

                    <div class="tab-pane fade active in" id="default-tab-4">
                        <div class="row" style="padding: 50px;">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'products-pjax']) ?>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-sm-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    Юридическая информация
                                                </h3>
                                                    <span class="pull-right">
                                                        <a class="btn btn-xs btn-warning" role="modal-remote" title="Изменить" href="<?=Url::toRoute(['edit-legal', 'user_id' => $user_id])?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    </span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">    
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold; text-align: center;">Наименование</td>
                                                            <td ><?=$legal_information->name?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold; text-align: center;">Текст</td>
                                                            <td ><?=$legal_information->text?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-12">
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-12">
                                    </div>
                                </div>
                            <?php Pjax::end() ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
