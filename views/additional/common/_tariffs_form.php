<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="common-form">

    <?php $form = ActiveForm::begin(); ?>    
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput([]) ?>
            </div>
            <div class="col-md-12">
                <div >                        
                    <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input">
                        + Добавить срок и цена
                    </button>
                        <div id="dynamic">
                        <br>
                            <?php
                                $data = json_decode($model->price_list, true);
                            ?>
                                <div class="row" style="margin-top:10px;" id="row<?=$k?>">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Срок</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <label>Цена</label>
                                    </div> 
                                </div>
                            <?php
                                foreach ($data as $price) {
                                    $k++;
                                ?>
                                <div class="row" style="margin-top:10px;" id="row<?=$k?>">
                                    <div class="col-md-6 col-xs-6"> 
                                        <input type="number" min="0" class="form-control" name="titles[]" value="<?=$price['time']?>" placeholder="Срок (месяц)"/> 
                                    </div>
                                    <div class="col-md-5 col-xs-5">
                                        <input type="number" min="0" class="form-control" name="prices[]" value="<?=$price['price']?>" placeholder="Цена (&#8381;)"/>
                                    </div> 
                                    <div class="col-md-1 col-xs-1" style="margin-left:-20px;" >
                                        <button type="button" name="remove" id="<?=$k?>" class="btn btn-danger btn_remove"> 
                                            <i class="glyphicon glyphicon-trash"></i> 
                                        </button> 
                                    </div>
                                </div>
                                <?php
                                } ?>
                                <input type="hidden" class="form-control" id="count" name="count" value="<?=$k?>"/> 
                        </div>
                    </div>
            </div>
        </div>
        <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <?php ActiveForm::end(); ?>
    
</div>
<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var i = document.getElementById('count').value;
    var fileCollection = new Array();
    $('#add_input').click(function(){
    i++;
    $('#dynamic').append('<div class="row" style="margin-top:10px;" id="row'+i+'"><div class="col-md-6 col-xs-6"> <input type="number" min="0" class="form-control" name="titles[]" placeholder="Срок (месяц)"/> </div><div class="col-md-5 col-xs-5"><input type="number" min="0" class="form-control" name="prices[]" placeholder="Цена (&#8381;)"/></div> <div class="col-md-1 col-xs-1" style="margin-left:-20px;" ><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

});
JS
);
?>