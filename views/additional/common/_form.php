<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

?>

<div class="common-form">

    <?php $form = ActiveForm::begin(); ?>    
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'timezone')->dropDownList($model->getTimezoneList(),[]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'pixel_id')->textInput(['type' => 'number', 'disabled' => ($tariff == 'pro' ? false : true), ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'currency')->dropDownList($model->getCurrencyList(),[]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'html_code')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                        'height' => '100px',
                    ],]);
                ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    
</div>
