<?php

use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

CrudAsset::register($this);
?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'blog-pjax']) ?>
<?php if($dostup) { ?>
	<center>
		<?= Html::a('Создать новый блог', ['/common/add-blog'], ['role'=>'modal-remote','title'=> 'Создать новый блог','class'=>'btn btn-primary']) ?>
	</center>
<?php } ?>

<div class="row" style="text-align: center;"> 
	<?php
		foreach ($blogs as $blog) 
		{	
			if($blog->image == null || $blog->image == '') $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-images.png';
			else $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/blog/'.$blog->image;
	?>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" style="margin-top: 30px;">
		<?php
		?>
		<a href="<?=Url::toRoute(['/common/view-blog', 'id' => $blog->id])?>" data-pjax="0">
			<img src="<?=$path?>" alt="" style="max-width: 350px; max-height: 240px;">
			<h4><?=$blog->title?></h4>
			<span>Читать дальше &nbsp; <i class="fa fa-angle-right"></i></span>
		</a>
	</div>  
	<?php
		}
	?>
</div>

<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
