<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

?>

<div class="common-form">

    <?php $form = ActiveForm::begin(); ?>    
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput([]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'text')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                        'height' => '100px',
                    ],]);
                ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    
</div>
