<?php
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;
use yii\widgets\Pjax;


CrudAsset::register($this);
?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'tariff-pjax']) ?>
<div class="common-form">
	<div class="has-text-centered has-mb-8"> 
		<h3>Цены и тарифы</h3>
		<br>
	</div>
	<div class="row">
		<?php 
			foreach ($tariffs as $tarif) {
		?>
		<div class="col-md-6 col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><?=$tarif->name?></h3>
					<span class="pull-right">
                        <?php 
                            if($dostup){
                        ?>
						<a class="btn btn-xs btn-warning" role="modal-remote" title="Изменить" href="<?=Url::toRoute(['edit-tariff', 'id' => $tarif->id])?>"><i class="glyphicon glyphicon-pencil"></i></a>
                        <?php } ?>
					</span>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
                        <table class="table table-bordered">    
                            <tr style="font-weight: bold; text-align: center;">
                                <td>Сроки</td>
                                <td>Цены</td>
                            </tr>
                        <?php 
                        	$data = json_decode($tarif->price_list, true);
                			foreach ($data as $value) 
                			{
                        ?>
                            <tr style="text-align: center;">
                                <td>
                                    <?=$value['time']?> Месяц
                                </td> 
                                <td>
                                    <?=$value['price']?> &#8381;
                                </td>
                            </tr>
                        <?php
                    		}
                        ?>                                                 
                        </table>
                    </div>
				</div>
			</div>
		</div>
		<?php
			}
		?>
	</div>    
</div>
<?php Pjax::end() ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
