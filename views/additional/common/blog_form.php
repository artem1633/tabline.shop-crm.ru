<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

?>

<div class="common-form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>    
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'title')->textInput([]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'file')->fileInput(); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'text')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                        'height' => '200px',
                    ],]);
                ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    
</div>
