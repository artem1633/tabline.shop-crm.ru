<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Common */
?>
<div class="common-update">

    <?= $this->render('_form', [
        'model' => $model,
        'tariff' => $tariff,
    ]) ?>

</div>
