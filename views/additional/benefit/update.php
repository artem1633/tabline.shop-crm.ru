<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Benefit */
?>
<div class="benefit-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
