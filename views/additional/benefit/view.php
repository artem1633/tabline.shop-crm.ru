<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Benefit */
?>
<div class="benefit-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'teg:ntext',
            'image',
        ],
    ]) ?>

</div>
