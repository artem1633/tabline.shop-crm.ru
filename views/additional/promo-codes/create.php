<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\additional\PromoCodes */

?>
<div class="promo-codes-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
