<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \app\models\Instruction $model */

?>

<?php $form = ActiveForm::begin() ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">Редактирование политика конфиденциальности</h4>
            <span class="pull-right">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-xs']) ?>
            </span>
        </div>
        <div class="panel-body">

            <?= $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::class, [
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '300px',
                ],
            ])->label('Контент') ?>

        </div>
    </div>
<?php ActiveForm::end() ?>
