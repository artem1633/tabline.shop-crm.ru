<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form" style="padding: 15px;">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    	<div class="col-md-12">
		    <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>    		
    	</div>
    	<div class="col-md-6">
    		<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>    		
    	</div>
    	<div class="col-md-6">
		    <?= $form->field($model, 'mailing_address')->textInput(['maxlength' => true]) ?>
    	</div>
    </div>

     <div class="row">
    	<div class="col-md-4">
    		<?= $form->field($model, 'state_registration')->textInput(['maxlength' => true]) ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'inn')->textInput(['type' => 'number']) ?>
    	</div>
    	<div class="col-md-4">
		    <?= $form->field($model, 'ogrn')->textInput(['type' => 'number']) ?>
    	</div>
    </div>

    <div class="row">
    	<div class="col-md-4">
    		<?= $form->field($model, 'r_s')->textInput(['type' => 'number']) ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'bik')->textInput(['type' => 'number']) ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'kor_schot')->textInput(['type' => 'number']) ?>    		
    	</div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
