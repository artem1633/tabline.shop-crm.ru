<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsersPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'page_number')->dropDownList($model->getPageList(Yii::$app->user->identity->id), ['prompt' => 'Выберите' ]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
