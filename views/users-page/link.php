<?php

use app\models\blocks\TextBlock;
use app\models\blocks\LinkBlock;
use app\models\blocks\SeparatorBlock;
use app\models\blocks\HtmlBlock;
use app\models\blocks\AvatarBlock;
use app\models\blocks\QuestionBlock;
use app\models\blocks\MapBlock;
use app\models\blocks\VideoBlock;
use app\models\blocks\UniqueSellingBlock;
use app\models\blocks\CaruselBlock;
use app\models\blocks\AdvantageBlock;
use app\models\blocks\SocialBlock;
use app\models\blocks\MessengerBlock;
use app\models\blocks\ContactFormBlock;
use app\models\blocks\ProductBlock;
use app\models\UsersPage;
use yii\base\DynamicModel;

use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;
use app\models\blocks\MapMarks;
use yii\helpers\Html;

$this->title = $user_page->link_name;
?>
<?php
foreach ($blocks as $block) {

	if($block->table_name == 'text_block') 
	{
		$textBlock = TextBlock::findOne($block->field_id);
	  	if($textBlock->visible) echo $this->render('blocks/_text_block', ['textBlock' => $textBlock]);
	}

	if($block->table_name == 'link_block') 
	{
		$linkBlock = LinkBlock::findOne($block->field_id);
	  	if($linkBlock->visible) echo $this->render('blocks/_link_block', ['linkBlock' => $linkBlock]);
	}

	if($block->table_name == 'separator_block') 
	{
		$separatorBlock = SeparatorBlock::findOne($block->field_id);
	  	echo $this->render('blocks/_separator_block', ['separatorBlock' => $separatorBlock]);
	}

	if($block->table_name == 'html_block') 
	{
		$htmlBlock = HtmlBlock::findOne($block->field_id);
	  	if($htmlBlock->visible) echo $this->render('blocks/_html_block', ['htmlBlock' => $htmlBlock]);
	}

	if($block->table_name == 'avatar_block') 
	{
		$avatarBlock = AvatarBlock::findOne($block->field_id);
	  	if($avatarBlock->visible) echo $this->render('blocks/_avatar_block', ['avatarBlock' => $avatarBlock]);
	}

	if($block->table_name == 'question_block') 
	{
		$questionBlock = QuestionBlock::find()->where(['number' => $block->field_id])->all();
		$visible = QuestionBlock::find()->where(['number' => $block->field_id])->one()->visible;
	  	if($visible) echo $this->render('blocks/_question_block', ['questionBlock' => $questionBlock]);
	}

	if($block->table_name == 'map_block') 
	{
		$mapBlock = MapBlock::findOne($block->field_id);
		$marks = [];
        $marks_list = MapMarks::find()->where(['map_id' => $mapBlock->id])->all();
        
        foreach ($marks_list as $value) {
            $marks [] = new Placemark(new Point($value->coordinate_x, $value->coordinate_y), 
                    [ 
                        'balloonContent' => '<strong>'.$value->title.'</strong>'
                        .' <br>'.$value->description, ], 
                    [
                        'draggable' => false,
                        'preset' => 'islands#dotIcon',
                        'iconColor' => $value->color,
                ] 
            );            
        } 

	  	if($mapBlock->visible) echo $this->render('blocks/_map_block', ['model' => $mapBlock, 'marks' => $marks,]);
	}

	if($block->table_name == 'video_block') 
	{
		$videoBlock = VideoBlock::findOne($block->field_id);
	  	if($videoBlock->visible) echo $this->render('blocks/_video_block', ['videoBlock' => $videoBlock]);
	}

	if($block->table_name == 'unique_selling_block') 
	{
		$uniqueSellingBlock = UniqueSellingBlock::findOne($block->field_id);
	  	if($uniqueSellingBlock->visible) echo $this->render('blocks/_unique_selling_block', ['uniqueSellingBlock' => $uniqueSellingBlock]);
	}

	if($block->table_name == 'carusel_block') 
	{
		$caruselBlock = CaruselBlock::find()->where(['number' => $block->field_id])->all();
		$visible = CaruselBlock::find()->where(['number' => $block->field_id])->one()->visible;
	  	if($visible) echo $this->render('blocks/_carusel_block', ['caruselBlock' => $caruselBlock]);
	}

	if($block->table_name == 'advantage_block') 
	{
		$advantageBlock = AdvantageBlock::find()->where(['number' => $block->field_id])->all();
		$visible = AdvantageBlock::find()->where(['number' => $block->field_id])->one()->visible;
	  	if($visible) echo $this->render('blocks/_advantage_block', ['advantageBlock' => $advantageBlock]);
	}

	if($block->table_name == 'social_block') 
	{
		$socialBlock = SocialBlock::findOne($block->field_id);
	  	if($socialBlock->visible) echo $this->render('blocks/_social_block', ['socialBlock' => $socialBlock]);
	}

	if($block->table_name == 'messenger_block') 
	{
		$messengerBlock = MessengerBlock::findOne($block->field_id);
	  	if($messengerBlock->visible) echo $this->render('blocks/_messenger_block', ['messengerBlock' => $messengerBlock]);
	}

	if($block->table_name == 'contact_form_block') 
	{
		$contactFormBlock = ContactFormBlock::findOne($block->field_id);
	  	if($contactFormBlock->visible) echo $this->render('blocks/_contact_form_block', ['model' => $contactFormBlock, /*'fieldsModel' => $fieldsModel*/ ]);
	}

	if($block->table_name == 'page_block') 
	{
		$usersPage = UsersPage::findOne($block->field_id);
	  	echo $this->render('blocks/_users_page', ['usersPage' => $usersPage]);
	}

	if($block->table_name == 'product_block') 
	{
		$productBlock = ProductBlock::find()->where(['number' => $block->field_id])->all();
		$visible = ProductBlock::find()->where(['number' => $block->field_id])->one()->visible;
        $user_id = ProductBlock::find()->where(['number' => $block->field_id])->one()->page->user_id;
        if($user_id == null) $user_id = Yii::$app->user->identity->id;

	  	if($visible) echo $this->render('blocks/_product_block', ['productBlock' => $productBlock, 'user_id' => $user_id]);
	}
}
?>