<?php
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="users-page-index">
    <div id="ajaxCrudDatatable">
		<?php $form = ActiveForm::begin(['id' => 'date-form']); ?>
			<div class="row">
				<div class="col-md-8" style="margin-left: 15%;">
					<?= SortableInput::widget([
					    'name'=>'active',
					    'items' => $active, 
					    'hideInput' => true,
					    'sortableOptions' => [
					        'connected'=>true,
					        'itemOptions'=>['class'=>'alert alert-success'],
					    ],
					    'options' => ['class'=>'form-control', 'readonly'=>true]
					])?>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
    </div>
</div>