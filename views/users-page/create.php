<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsersPage */

?>
<div class="users-page-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
