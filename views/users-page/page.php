<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use kartik\sortinput\SortableInput;
use yii\widgets\ActiveForm;

CrudAsset::register($this);
$this->title = 'Страница';
$this->params['breadcrumbs'][] = $this->title;

if($active == null) $disabled = 'display:none;';
else $disabled = '1';

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'page-pjax']) ?>
<div class="users-page-index">
	
	<?php if($model == null && $user_id != null){ ?>
			<center>
				<?= Html::a('Создать новую страницу', ['add', 'user_id' => $user_id, 'active' => 1, 'is_main' => 1], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) ?>
			</center>
	<?php }
	if($model != null){ ?>
			<div class="row"> 
				<div class="col-lg-12"> 
					<div class="btn" style="width:100%; background-color: #f5f5f5;"> 
						<div class="pull-left">
							<span class="is-hidden-mobile">Моя ссылка:  </span>
							<?= Html::a('<span class="is-hidden-mobile">http://'. $_SERVER['SERVER_NAME'].'/'.$model->link_name .'	</span>', ['/'.$model->link_name], ['data-pjax' => '0', 'target'=> "_blank"]) ?>
						</div>
						<div class="pull-right">
							<?= Html::a('Действия', ['view', 'user_id' => $user_id,], [ 'style' => 'background-color:#08e4fd; color:black;', 'role'=>'modal-remote','title'=> 'Добавить страницу','class'=>'btn btn-xs btn-primary ',]) ?>
							</div>
					</div>  
				</div> 
			</div>

		    <div id="ajaxCrudDatatable">
		    	<div class='main-block main-block-xs-clear'>
				   	<center>
				        <div class="marvel-device iphone6 silver device-xs-hide">
				            <div class=top-bar></div>
				            <div class=sleep></div>
				            <div class=volume></div>
				            <div class=camera></div>
				            <div class=sensor></div>
				            <div class=speaker></div>
				            <div class="screen page">
				            	<?php $form = ActiveForm::begin(); ?>
									<div class="clr">
										<?= SortableInput::widget([
										    'name'=>'active',
										    'id'=>'actives',
										    'items' => $active, 
										    //'value' => '13,14,15,16,17,18',
										    'hideInput' => true,
										    'sortableOptions' => [
										        'connected'=>true,
										        'itemOptions'=>['class'=>'alert alert-success'],
										    ],
										    'options' => ['class'=>'form-control', 'readonly'=>true]
										])?>
										<div class="col-md-12" style="<?=$disabled?>" >
										    <?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-success', 'style' => 'width:100%;']) ?>
										</div>
										<div class="col-md-12">
											<br>
											<?= Html::a('Добавить новый блок', ['add-block', 'page_id' => $model->id], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary', 'style' => 'width:100%;']) ?>
											<div class="col-md-12" style="">
												<br>
											</div>
										</div>
									</div>
								<?php ActiveForm::end(); ?>
				            </div>
				            <div class=home></div>
				            <div class=bottom-bar></div>
				        </div>
				   	</center>
				</div>				
		    </div>
	<?php } ?>

<?php Pjax::end() ?>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>