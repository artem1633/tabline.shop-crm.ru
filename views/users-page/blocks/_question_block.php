<?php
use yii\helpers\Html;
?>

<div class="question-block" id="question_block" >
	<div style="text-align:center; font-weight:bold;">
        <?= $questionBlock[0]->title_text ?>
    </div>
	<?php 
		foreach ($questionBlock as $question) {
	?>
	<div class="equations">
	  <b><?=$question->question_title?></b>
	  <p class="answers"><br><?=$question->answer?></p>
	</div>
	<?php }
	?>
</div>