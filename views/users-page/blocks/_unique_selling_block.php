<?php
use yii\helpers\Html;

if (!file_exists('images/unique-selling-block/'.$uniqueSellingBlock->image) || $uniqueSellingBlock->image == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-images.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/unique-selling-block/'.$uniqueSellingBlock->image;
}
?>

<div class="unical" id="unique_selling_block" style="background-image: url(<?=$path?>);" >
	<div style="text-align:center; font-weight:bold;">
        <?= $uniqueSellingBlock->title_text ?>
    </div>
   <h3><?=$uniqueSellingBlock->title?></h3>
   <p><?=$uniqueSellingBlock->text?></p>
   <a href="<?= 'http://' .  $uniqueSellingBlock->link?>" target="_blank"><?=$uniqueSellingBlock->button_text?></a>
</div>