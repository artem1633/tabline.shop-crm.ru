<?php

?>
<div class="advantage-block" id="advantage_block">
    <div style="text-align:center; font-weight:bold;">
        <?= $advantageBlock[0]->title_text ?>
    </div>
    <div id="myCarouselAdvantage" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php 
            $i = -1;
            foreach ($advantageBlock as $value) {
                $i++;
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/advantage-block/' . $value->number . '/' . $value->image;
            ?>
            <li data-target="#myCarouselAdvantage" data-slide-to="<?=$i?>" <?= $i == 0 ? 'class="active"' : '' ?> ></li>
            <?php } ?>
        </ol>
    
        <div class="carousel-inner" style="height: auto; max-height: 400px;">
            <?php 
            $i = 0;
            foreach ($advantageBlock as $value) {
                $i++;
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/advantage-block/' . $value->number . '/' . $value->image;
            ?>
                <div class="item <?= $i == 1 ? 'active' : '' ?> " style="background-image: url('<?=$path?>');" >
                </div>
            <?php } ?>
        </div>
    
        <a class="left carousel-control" href="#myCarouselAdvantage" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarouselAdvantage" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
  </div>
</div>