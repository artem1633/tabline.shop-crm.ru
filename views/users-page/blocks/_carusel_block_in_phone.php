<?php

?>
<div class="carusel-block">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php 
            $i = -1;
            foreach ($caruselBlock as $value) {
                $i++;
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/carusel-block/' . $value->number . '/' . $value->image;
            ?>
            <li data-target="#myCarousel" data-slide-to="<?=$i?>" <?= $i == 0 ? 'class="active"' : '' ?> ></li>
            <?php } ?>
        </ol>
    
        <div class="carousel-inner">
            <?php 
            $i = 0;
            foreach ($caruselBlock as $value) {
                $i++;
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/carusel-block/' . $value->number . '/' . $value->image;
            ?>
                <div class="item <?= $i == 1 ? 'active' : '' ?> " style=" height: <?=$value->getSizeDescription()?>px; background-image: url('<?=$path?>');" >
                </div>
            <?php } ?>
        </div>
    </div>
</div>