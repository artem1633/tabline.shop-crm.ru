<?php
use yii\helpers\Html;
use app\models\blocks\ProductBlock;
use app\models\additional\Common;

$currency = Common::find()->where(['user_id' => $user_id])->one()->getCurrencyFont();
?>
<input type="hidden" class="form-control" id="product_id" name="product_id" value="<?=$productBlock[0]->id?>"/> 
<div style="text-align:center; font-weight:bold;">
    <?= $productBlock[0]->title_text ?>
</div>
<?php if($productBlock[0]->view == 'slider') { ?>
<div class="product-block" id="product_block">
    <div id="myCarouselProduct" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php 
            $i = -1;
            foreach ($productBlock as $value) {
                $i++;
            ?>
            <li data-target="#myCarouselProduct" data-slide-to="<?=$i?>" <?= $i == 0 ? 'class="active"' : '' ?> ></li>
            <?php } ?>
        </ol>
    
        <div class="carousel-inner">
            <?php 
            $i = 0;
            foreach ($productBlock as $value) {
                $i++;
                $data = json_decode($value->product->photos, true);
                $path = "http://". $_SERVER['SERVER_NAME'] . '/' . $data[0]['path'];
                if($value->product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $value->product->last_price . ' ' . $currency . '</s> ';
                else $last = '';
            ?>
                <div class="item <?= $i == 1 ? 'active' : '' ?> " >
                    <center>
                        <?php if($value->foto_visible){ ?>
                            <img style="height: 160px; width: 250px; margin-top: 50px;" src="<?=$path?>">
                        <?php }
                        if($value->name_visible){ ?>
                            <div style="margin-top: 10px; font-size: 20px;">
                                <?=$value->product->name?>
                            </div>
                        <?php }
                        if($value->description_visible){ ?>
                            <div style="color: black; margin-top: 10px; font-size: 14px; width: 300px;">
                                <?=$value->product->description?>
                            </div>
                        <?php }
                        if($value->price_visible){ ?>
                            <div style="margin-top: 10px; font-size: 16px; font-weight: bold;">
                                <?= $last . $value->product->price . ' ' . $currency;?>
                            </div>
                        <?php }
                        if($value->button_visible){ ?>
                            <div style="margin-top: 10px;">
                                <a onclick = "alert('Продукт успешно добавлен на корзину!'); $.post('/products/set-product?product_id=<?=$value->product->id?>&count=1'); /*$.pjax.reload({container:'#cash-pjax', async: false});*/ " class="btn btn-warning">Купить</a>
                            </div>
                        <?php } ?>
                    </center>
                </div>

            <?php } ?>
        </div>
    
        <a class="left carousel-control" href="#myCarouselProduct" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarouselProduct" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<?php } ?>
<?php if($productBlock[0]->view == 'table') { ?>
    <div class="table-responsive">
        <table class="table table-bordered">
            <?php 
            $i = 0;
            foreach ($productBlock as $value) {
                $i++;
                $data = json_decode($value->product->photos, true);
                if($value->product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $value->product->last_price . ' ' . $currency . '</s> ';
                else $last = '';
            ?>
                <tr>
                    <td>                        
                        <div class="advantage-block">
                            <div id="myCarouselPro" class="carousel slide" data-ride="carousel">                        
                                <div class="carousel-inner">
                                    <?php 
                                    $i = 0;
                                    foreach ($data as $image) {
                                        $i++;
                                        $path = "http://". $_SERVER['SERVER_NAME'] . '/' . $image['path'];
                                    ?>
                                        <div class="item <?= $i == 1 ? 'active' : '' ?> " >
                                            <?php if($value->foto_visible){ ?>
                                                <center><img style="height: 160px; width: 180px;" src="<?=$path?>"></center>
                                            <?php } ?>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div style=" font-size: 16px; font-weight: bold; text-align: center;">
                            <?php if($value->price_visible){ ?>
                                <?= $last . $value->product->price . ' ' . $currency . '&nbsp;&nbsp;';?>
                            <?php }
                                if($value->button_visible){ ?>
                                    <a onclick = "alert('Продукт успешно добавлен на корзину!'); $.post('/products/set-product?product_id=<?=$value->product->id?>&count=1'); /*$.pjax.reload({container:'#cash-pjax', async: false});*/ " class="btn btn-warning btn-xs">Купить</a>
                            <?php } ?>
                        </div>
                    </td> 
                   <td>
                    <center>
                        <?php if($value->name_visible){ ?>
                            <div style=" margin-top: 10px; font-size: 20px;font-weight: bold;">
                                <?=$value->product->name?>
                            </div>
                        <?php }
                        if($value->description_visible){ ?>
                            <div style="color: black; margin-top: 10px; font-size: 14px;">
                                <?=$value->product->description?>
                            </div>
                        <?php } ?>
                    </center>
                   </td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php } ?>
<?php if($productBlock[0]->view == 'list') { ?>
<div class="product-block" id="product_block">
    <select class="form-control" id="products" name="products[]"><?= ProductBlock::ProductsList($productBlock)?></select>
    <br>
    <div id="goods">        
    </div>
</div>
<?php } ?>

<?php 
$this->registerJs(<<<JS

$('#products').on('change', function() 
    {  
        var product = this.value;
        var product_id = document.getElementById('product_id').value;

        $.post( "/products/get-product-cart?id="+product+"&product_id="+product_id, function( data ){
            $( "#goods" ).html( data);
        });

    }
    );
JS
);
?>