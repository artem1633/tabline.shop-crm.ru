<?php

use yii\helpers\Html;


if($linkBlock->action_click == 'site_url') $url = 'http://' . $linkBlock->site_url;
if($linkBlock->action_click == 'call') $url = 'http://' . $linkBlock->call;
if($linkBlock->action_click == 'email') $url = 'http://' . $linkBlock->email;
if($linkBlock->action_click == 'other_page_name') $url = 'http://' . $linkBlock->other_page_name;

$tekst = Html::a( $linkBlock->caption, $url, [ 'onclick'=> "$.post('/users-page/set-click?table_name=link_block&id=".$linkBlock->id."');",'style'=>'font-size:18px; font-weight:bold;','data-pjax'=>'0','target' => '_blank',]);

?>

<div class="link-block" id="link_block" style="text-align:center;">
	<div style="text-align:center; font-weight:bold;">
        <?= $linkBlock->title_text ?>
    </div>
 	<?=$tekst?>
</div>