<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;
use app\models\UsersPage;
use app\models\additional\Permissions;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tariff_id',
        'content' => function($data){
            return $data->tariff->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'registry_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_payment_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'end_access',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_payment_amount',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function($data){
            return $data->getTypeDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'paternity_user_code',
        'content' => function($data){
            if($data->type == 3) return Users::find()->where([ 'paternity_user_code' => $data->partner_code ])->count();
            else return null;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'page',
        'visible' => function($data){
            if($data->type == 2) {
                $permission = Permissions::find()->where(['user_id' => Yii::$app->user->identity->id ])->one();
                if(!$permission->view_page) return false;
            }
            return true;
        },
        'content' => function($data){
            $pages = UsersPage::find()->where(['user_id' => $data->id])->orderBy(['is_main' => SORT_DESC])->all();
            $string = '';
            foreach ($pages as $page) {
                $string .=  Html::a('http://' . $_SERVER['SERVER_NAME'] . '/' . $page->link_name, 'http://' . $_SERVER['SERVER_NAME'] . '/' . $page->link_name, ['target' => '_blank', 'data-pjax' => 0]) . ' <br>';
            }
            return $string;
        }
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{leadView} {leadUpdate} {leadDelete}',
        'buttons'  => [
            'leadView' => function ($url, $model) {
                $url = Url::to(['/users/view-in-table', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['role'=>'modal-remote','title'=>'Просмотр', 'data-toggle'=>'tooltip']);
            },
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/users/update', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                if($model->type != 0) {
                    $type = Yii::$app->user->identity->type;
                    $user_id = Yii::$app->user->identity->id;
                    $display = true;
                    if($type == 2)
                    {
                        $permission = Permissions::find()->where(['user_id' => $user_id])->one();
                        if(!$permission->delete_user) $display = false;
                    }

                    /*if(!$display)
                    {*/
                        $url = Url::to(['/users/delete', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'role'=>'modal-remote','title'=>'Удалить', 
                            'data-confirm'=>false, 'data-method'=>false,
                            'data-request-method'=>'post',
                            'data-toggle'=>'tooltip',
                            'style' => ($type == 2) ? $display : true,
                            'data-confirm-title'=>'Подтвердите действие',
                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                        ]);
                    /*}*/
                }
            },
        ]
    ]

];   