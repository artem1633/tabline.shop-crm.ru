<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\additional\Permissions;

$type = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;
$disabled = false;
if($type == 2) {
    $permission = Permissions::find()->where(['user_id' => $user_id])->one();
    if(!$permission->change_tariff && !$model->isNewRecord) $disabled = true;
}

if (!file_exists('avatars/'.$model->avatar) || $model->avatar == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->avatar;
}

?>

<div class="users-form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <center>
            <?=Html::img($path, [
                'style' => 'width:180px; height:180px;',
                'class' => 'img-circle',
            ])?>
            </center>
        </div>
        <div class="col-md-8 col-xs-12">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'type')->dropDownList($model->getType(), ['prompt' => 'Выберите тип', 'disabled' => ($model->isNewRecord ? false : $model->type == 0 ? true : false) ]) ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'file')->fileInput(); ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'tariff_id')->dropdownList($model->getTariffList(), ['disabled' => $disabled]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'end_access')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату','disabled' => $disabled],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
                ])
            ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'last_payment_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату',],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
                ])
            ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
