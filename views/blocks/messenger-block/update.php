<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\MessengerBlock */
?>
<div class="messenger-block-update">

    <?= $this->render('_form', [
        'model' => $model,
        'day_count' => $day_count,
        'week_count' => $week_count,
        'month_count' => $month_count,
    ]) ?>

</div>
