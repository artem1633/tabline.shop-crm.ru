<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>
<?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Картинки</b></span>
                        <span class="hidden-xs"><b>Картинки</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Статистика</b></span>
                        <span class="hidden-xs"><b>Статистика</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>                    

                        <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input"> + Добавить новый слайд</button>
                        <div class="row">
                        <br>
                            <div class="col-md-6">
                                <?= $form->field($model, 'size_slide')->dropDownList($model->getSlideSizeList(), []) ?>            
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'time')->textInput(['type' => 'number']) ?>
                            </div>
                        </div>
                        <div id="dynamic"></div>

                        <?php if (!Yii::$app->request->isAjax){ ?>
                            <div class="form-group">
                                <?= Html::submitButton('Создать', ['class' =>  'btn btn-success']) ?>
                            </div>
                        <?php } ?>

                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-3">
                    <br>
                    <div class="row">
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">День</h3>
                                    <span class="pull-right"><?=date('d.m.Y')?></span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$day_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Неделя</h3>
                                    <span class="pull-right">
                                        <?=date('d.m.Y', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) ). ' - ' .date('d.m.Y')?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$week_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Месяц</h3>
                                    <span class="pull-right">
                                        <?= date('01.m.Y') . ' - ' .date('d.m.Y') ?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$month_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var i=0;
    var fileCollection = new Array();
    $('#add_input').click(function(){
    i++;
    $('#dynamic').append('<div class="row" id="row'+i+'"><div class="col-md-12"><label class="control-label" ><br>Слайд'+i+' </label></div> <div class="col-md-11"> <input type="text" class="form-control" name="titles[]" placeholder="Заголовок"/> </div> <div class="col-md-1" style="margin-left:-20px;" ><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> </div> <div class="col-md-11"><br><textarea  class="form-control" name="texts[]" placeholder="Описание"></textarea></div><div class="col-md-11"><br><div id="images-to-upload'+i+'"></div><div class="col-md-12"><br><input type="file" accept=".svg,.jpg,.jpeg,.png" name="images[]" class="btn_image"  id="'+i+'"></div></div></div>');
    });

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

    $(document).on('change', '.btn_image', function(e){
        var files = e.target.files;
        var button_id = $(this).attr("id");
        //alert(button_id);
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%;" src="'+e.target.result+'"> ';
                $('#images-to-upload'+button_id+'').html('');
                //document.getElementById('theID').value = 'new value';
                $('#images-to-upload'+button_id+'').append(template);
            };
        });
    });

});
JS
);
?>