<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\ProductBlock */
?>
<div class="product-block-update">

    <?= $this->render('_form', [
        'products' => $products,
        'number' => $number,
        'count' => $count,
        'model' => $model,
    ]) ?>

</div>
