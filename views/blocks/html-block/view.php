<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\HtmlBlock */
?>
<div class="html-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'text:ntext',
        ],
    ]) ?>

</div>
