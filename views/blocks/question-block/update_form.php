<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Вопросы</b></span>
                        <span class="hidden-xs"><b>Вопросы</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                   	<div id="dynamic">
				    	<?php
				    	$k=0;
				    	foreach ($questions as $question) {
				    		$k++;
				    	?>
				    	<div class="row" id="row<?=$k?>">
				    		<div class="col-md-12">
				    			<label class="control-label" ><br>Вопрос </label>
				    		</div> 
				    		<div class="col-md-11"> 
				    			<input type="text" required class="form-control" value="<?=$question->question_title?>" name="name[]" placeholder="Заголовок"/> 
				    		</div> 
				    		<div class="col-md-1" style="margin-left:-20px;" >
				    			<button type="button" name="remove" id="<?=$k?>" class="btn btn-danger btn_remove"> 
				    				<i class="glyphicon glyphicon-trash"></i> 
				    			</button> 
				    		</div> 
				    		<div class="col-md-11">
				    			<br>
				    			<textarea class="form-control" name="answer[]" placeholder="Ответ на вопрос"><?=$question->answer?></textarea>
				    		</div>
				    	</div>
				    	<?php
				    	} ?>
				    </div>

					<br>
				    <button type="button" class="btn btn-primary btn-xs" name="add" id="add_input">Добавить новый пункт</button>
				    <input id="number" name="number" type="hidden" value="<?=$number?>">
				    <input id="count" name="count" type="hidden" value="<?=$count?>">
  
                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS
$(document).ready(function(){
	var i = document.getElementById('count').value;
 	$('#add_input').click(function(){
 	i++;
 	$('#dynamic').append('<div class="row" id="row'+i+'"><div class="col-md-12"><label class="control-label" ><br>Вопрос </label></div> <div class="col-md-11"> <input type="text" required class="form-control" name="name[]" placeholder="Заголовок"/> </div> <div class="col-md-1" style="margin-left:-20px;" ><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> </div> <div class="col-md-11"><br><textarea  class="form-control" name="answer[]" placeholder="Ответ на вопрос"></textarea></div></div>');
 	});

	$(document).on('click', '.btn_remove', function(){
 		var button_id = $(this).attr("id");
 		$('#row'+button_id+'').remove();
 	});
});
JS
);
?>