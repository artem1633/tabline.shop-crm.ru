<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\AvatarBlock */
?>
<div class="avatar-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
