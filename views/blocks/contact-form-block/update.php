<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\ContactFormBlock */
?>
<div class="contact-form-block-update">

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
