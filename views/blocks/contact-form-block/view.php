<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\ContactFormBlock */
?>
<div class="contact-form-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'name',
            'name_title',
            'name_description',
            'telephone',
            'telephone_title',
            'telephone_description',
            'email:email',
            'email_title:email',
            'email_description:email',
            'line',
            'line_title',
            'line_description:ntext',
            'number',
            'number_title',
            'number_description',
            'list',
            'list_title',
            'list_description:ntext',
            'check',
            'check_title',
            'check_description',
            'check_selected',
            'select',
            'select_title',
            'select_description:ntext',
            'text',
            'text_title',
            'text_description',
            'country',
            'country_title',
            'country_description',
        ],
    ]) ?>

</div>
