<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\LinkBlock */
?>
<div class="link-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'caption',
            'action_click',
            'site_url:url',
            'call',
            'email:email',
            'other_page_name',
        ],
    ]) ?>

</div>
