<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\UniqueSellingBlock */
?>
<div class="unique-selling-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'title',
            'text:ntext',
            'image',
            'button_text',
            'link',
        ],
    ]) ?>

</div>
