<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\UniqueSellingBlock */
?>
<div class="unique-selling-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
