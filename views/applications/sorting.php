<?php
use kartik\sortinput\SortableInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(); ?>
	<div class="panel panel-primary">

	    <div class="panel-heading">
			<h4 class="panel-title">Настройка столбцов</h4>
			<span class="pull-right"><?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-xs btn-success']) ?>	</span>
		</div>

	    <div class="panel-body">
			<div class="row">
				<div class="col-sm-6">
					<label><h3 style="color:blue; font-weight:bold;"> Активно </h3></label>
					<?=SortableInput::widget([
					    'name'=>'active',
					    'items' => $active, 
					    'hideInput' => true,
					    'sortableOptions' => [
					        'connected'=>true,
					        'itemOptions'=>['class'=>'alert alert-success'],
					    ],
					    'options' => ['class'=>'form-control', 'readonly'=>true]
					])?>
				</div>

				<div class="col-sm-6">
					<label><h3 style="color:red; font-weight:bold;"> Неактивно </h3></label>
					<?=SortableInput::widget([
					    'name'=>'no-active',
					    'items' => $noactive, 
					    'hideInput' => true,
					    'sortableOptions' => [
					        'itemOptions'=>['class'=>'alert alert-warning'],
					        'connected'=>true,
					    ],
					    'options' => ['class'=>'form-control', 'readonly'=>true]
					])
					?>
				</div>
			</div>
		</div>
	</div>
<?php ActiveForm::end(); ?>