<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsersAccess */
?>
<div class="users-access-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
