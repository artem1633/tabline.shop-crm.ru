<?php

use yii\db\Migration;

/**
 * Handles adding type to table `users_page`.
 */
class m180829_070527_add_type_column_to_users_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users_page', 'type', $this->integer()->defaultValue(0)->comment('Тип страниц'));
        $this->addColumn('users_page', 'description', $this->text()->comment('Описание'));
        $this->addColumn('users_page', 'image', $this->string(255)->comment('Фото'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users_page', 'type');
        $this->dropColumn('users_page', 'description');
        $this->dropColumn('users_page', 'image');
    }
}
