<?php

use yii\db\Migration;

/**
 * Handles the creation of table `html_block`.
 */
class m180716_084919_create_html_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('html_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'text' => $this->text(),
        ]);

        $this->createIndex('idx-html_block-page_id', 'html_block', 'page_id', false);
        $this->addForeignKey("fk-html_block-page_id", "html_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-html_block-page_id','html_block');
        $this->dropIndex('idx-html_block-page_id','html_block');
        
        $this->dropTable('html_block');
    }
}
