<?php

use yii\db\Migration;

/**
 * Class m180911_151305_add_values_to_settings_table
 */
class m180911_151305_add_values_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'name' => 'Google приложения',
            'key' => 'google_client_id',
            'value' =>'460452831567-1nsk9i61endg0ur5vlgtbg6n4j018aj7.apps.googleusercontent.com',
        ));

        $this->insert('settings',array(
            'name' => 'Google Защищённый ключ',
            'key' => 'google_client_secret_key',
            'value' =>'nJWXEEmIHvpENvCP7fVIEKwk',
        ));

        $this->insert('settings',array(
            'name' => 'Google Api Callback',
            'key' => 'google_redirect_uri',
            'value' =>'http://tabline.teo-control.ru/site/google',
        ));

        ///-------------------------------------------------------------------------------------///

        $this->insert('settings',array(
            'name' => 'Facebook приложения',
            'key' => 'facebook_client_id',
            'value' =>'225377498106610',
        ));

        $this->insert('settings',array(
            'name' => 'Facebook Защищённый ключ',
            'key' => 'facebook_client_secret',
            'value' =>'8e9698070e36c04bc0b73053e1a1add5',
        ));

        $this->insert('settings',array(
            'name' => 'Facebook Api Callback',
            'key' => 'facebook_redirect_uri',
            'value' =>'http://tabline.teo-control.ru/site/facebook',
        ));
    }

    public function down()
    {

    }
}
