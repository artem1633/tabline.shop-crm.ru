<?php

use yii\db\Migration;

/**
 * Handles the creation of table `write`.
 */
class m180901_161537_create_write_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('write', [
            'id' => $this->primaryKey(),
            'sender_name' => $this->string(255)->comment('Как к вам обращаться'),
            'sender_email' => $this->string(255)->comment('Как с вами связаться, email'),
            'text' => $this->text()->comment('Вопрос/предложение'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('write');
    }
}
