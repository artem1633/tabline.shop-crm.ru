<?php

use yii\db\Migration;
use app\models\Users;
use app\models\additional\Common;

/**
 * Class m180912_041602_add_default_values_to_common_table
 */
class m180912_041602_add_default_values_to_common_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $users = Users::find()->all();
        foreach ($users as $user) 
        {
            if(Common::find()->where(['user_id' => $user->id])->one() == null)
            {
                $common = new Common();
                $common->user_id = $user->id;
                $common->timezone = 3;
                $common->currency = 1;
                $common->save();                
            }
        }
    }

    public function down()
    {

    }
}
