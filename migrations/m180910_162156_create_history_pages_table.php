<?php

use yii\db\Migration;

/**
 * Handles the creation of table `history_pages`.
 */
class m180910_162156_create_history_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('history_pages', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'date' => $this->date()->comment('Дата'),
            'page' => $this->string(255)->comment('Страница/Ссылка'),
            'status' => $this->string(255)->comment('Статус'),
        ]);

        $this->createIndex('idx-history_pages-user_id', 'history_pages', 'user_id', false);
        $this->addForeignKey("fk-history_pages-user_id", "history_pages", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-history_pages-user_id','history_pages');
        $this->dropIndex('idx-history_pages-user_id','history_pages');

        $this->dropTable('history_pages');
    }
}
