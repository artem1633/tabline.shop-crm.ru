<?php

use yii\db\Migration;

/**
 * Handles the creation of table `legal_information`.
 */
class m180822_133517_create_legal_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('legal_information', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'text' => $this->text(),
        ]);

        $this->createIndex('idx-legal_information-user_id', 'legal_information', 'user_id', false);
        $this->addForeignKey("fk-legal_information-user_id", "legal_information", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-legal_information-user_id','legal_information');
        $this->dropIndex('idx-legal_information-user_id','legal_information');
        
        $this->dropTable('legal_information');
    }
}
