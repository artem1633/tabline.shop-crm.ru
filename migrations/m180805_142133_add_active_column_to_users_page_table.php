<?php

use yii\db\Migration;
use app\models\UsersPage;

/**
 * Handles adding active to table `users_page`.
 */
class m180805_142133_add_active_column_to_users_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users_page', 'active', $this->boolean());
        $this->addColumn('users_page', 'is_main', $this->boolean()->defaultValue(0));

        $pages = UsersPage::find()->all();
        foreach ($pages as $value) {
            $value->active = 1;
            $value->is_main = 1;
            $value->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users_page', 'active');
        $this->dropColumn('users_page', 'is_main');
    }
}
