<?php

use yii\db\Migration;

/**
 * Handles the creation of table `link_block`.
 */
class m180715_141313_create_link_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('link_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'caption' => $this->string(255),
            'action_click' => $this->string(50),
            'site_url' => $this->string(255),
            'call' => $this->string(255),
            'email' => $this->string(255),
            'other_page_name' => $this->string(255),
        ]);

        $this->createIndex('idx-link_block-page_id', 'link_block', 'page_id', false);
        $this->addForeignKey("fk-link_block-page_id", "link_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-link_block-page_id','link_block');
        $this->dropIndex('idx-link_block-page_id','link_block');
        
        $this->dropTable('link_block');
    }
}
