<?php

use yii\db\Migration;

/**
 * Handles the creation of table `carusel_block`.
 */
class m180721_060300_create_carusel_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('carusel_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'number' => $this->integer(),
            'size_slide' => $this->string(255),
            'time' => $this->integer(),
            'image' => $this->string(255),
            'title' => $this->string(255),
            'text' => $this->text(),
        ]);

        $this->createIndex('idx-carusel_block-page_id', 'carusel_block', 'page_id', false);
        $this->addForeignKey("fk-carusel_block-page_id", "carusel_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-carusel_block-page_id','carusel_block');
        $this->dropIndex('idx-carusel_block-page_id','carusel_block');
        
        $this->dropTable('carusel_block');
    }
}
