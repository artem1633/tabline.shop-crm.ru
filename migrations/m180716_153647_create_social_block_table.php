<?php

use yii\db\Migration;

/**
 * Handles the creation of table `social_block`.
 */
class m180716_153647_create_social_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('social_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'display' => $this->string(255),
            'facebook' => $this->boolean(),
            'facebook_link' => $this->string(255),
            'facebook_title' => $this->string(255),
            'instagram' => $this->boolean(),
            'instagram_link' => $this->string(255),
            'instagram_title' => $this->string(255),
            'vk' => $this->boolean(),
            'vk_link' => $this->string(255),
            'vk_title' => $this->string(255),
            'odno' => $this->boolean(),
            'odno_link' => $this->string(255),
            'odno_title' => $this->string(255),
            'twitter' => $this->boolean(),
            'twitter_link' => $this->string(255),
            'twitter_title' => $this->string(255),
            'youtube' => $this->boolean(),
            'youtube_link' => $this->string(255),
            'youtube_title' => $this->string(255),
            'pinterest' => $this->boolean(),
            'pinterest_link' => $this->string(255),
            'pinterest_title' => $this->string(255),
            'snapchat' => $this->boolean(),
            'snapchat_link' => $this->string(255),
            'snapchat_title' => $this->string(255),
        ]);

        $this->createIndex('idx-social_block-page_id', 'social_block', 'page_id', false);
        $this->addForeignKey("fk-social_block-page_id", "social_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-social_block-page_id','social_block');
        $this->dropIndex('idx-social_block-page_id','social_block');

        $this->dropTable('social_block');
    }
}
