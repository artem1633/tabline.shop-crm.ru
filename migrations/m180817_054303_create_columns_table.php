<?php

use yii\db\Migration;

/**
 * Handles the creation of table `columns`.
 */
class m180817_054303_create_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('columns', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'table_name' => $this->string(255)->comment('Таблица'),
            'type' => $this->string(255)->comment('Поля'),
            'name' => $this->string(255)->comment('Наименование'),
            'status' => $this->integer()->comment('Статус'),
            'order_number' => $this->integer()->comment('Сортировка'),
        ]);

        $this->createIndex('idx-columns-user_id', 'columns', 'user_id', false);
        $this->addForeignKey("fk-columns-user_id", "columns", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-columns-user_id','columns');
        $this->dropIndex('idx-columns-user_id','columns');

        $this->dropTable('columns');
    }
}
