<?php

use yii\db\Migration;

/**
 * Handles adding end_access to table `users`.
 */
class m180827_072042_add_end_access_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'end_access', $this->date());
        $this->addColumn('users', 'relation', $this->integer());
        $this->addColumn('users', 'tariff_id', $this->integer());

        $this->createIndex('idx-users-tariff_id', 'users', 'tariff_id', false);
        $this->addForeignKey("fk-users-tariff_id", "users", "tariff_id", "tariffs", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users-tariff_id','users');
        $this->dropIndex('idx-users-tariff_id','users');
        
        $this->dropColumn('users', 'end_access');
        $this->dropColumn('users', 'relation');
        $this->dropColumn('users', 'tariff_id');

    }
}
