<?php

use yii\db\Migration;

/**
 * Handles the creation of table `applications`.
 */
class m180815_175214_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->comment('Страница'),
            'place' => $this->string(255)->comment('Место '),
            'date' => $this->datetime()->comment('Дата заявки'),
            'status' => $this->string(20)->comment('Статус'),
            'account_number' => $this->integer()->comment('Номер счета '),
            'budget' => $this->float()->comment('Бюджет'),
            'pay' => $this->string(50)->comment('Оплата'),
            'purpose_of_payment' => $this->string(50)->comment('Назначение платежа'),
            'pay_type' => $this->string(50)->comment('Тип оплаты'),
            'pay_date' => $this->datetime()->comment('Дата оплаты'),
            'utm' => $this->string(255)->comment('UTM-метки'),
            'name' => $this->string(255)->comment('Имя'),
            'contacts' => $this->text()->comment('Контакты'),
            'fields' => $this->text()->comment('Полей'),
        ]);

        $this->createIndex('idx-applications-page_id', 'applications', 'page_id', false);
        $this->addForeignKey("fk-applications-page_id", "applications", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-applications-page_id','applications');
        $this->dropIndex('idx-applications-page_id','applications');

        $this->dropTable('applications');
    }
}
