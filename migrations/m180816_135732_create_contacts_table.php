<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180816_135732_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'email' => $this->string(255),
            'phone' => $this->string(255),
        ]);

        $this->createIndex('idx-contacts-user_id', 'contacts', 'user_id', false);
        $this->addForeignKey("fk-contacts-user_id", "contacts", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-contacts-user_id','contacts');
        $this->dropIndex('idx-contacts-user_id','contacts');
        
        $this->dropTable('contacts');
    }
}
