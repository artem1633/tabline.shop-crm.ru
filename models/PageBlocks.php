<?php

namespace app\models;

use Yii;

use app\models\blocks\TextBlock;
use app\models\blocks\LinkBlock;
use app\models\blocks\HtmlBlock;
use app\models\blocks\AvatarBlock;
use app\models\blocks\SocialBlock;
use app\models\blocks\VideoBlock;
use app\models\blocks\MessengerBlock;
use app\models\blocks\QuestionBlock;
use app\models\blocks\CaruselBlock;
use app\models\blocks\AdvantageBlock;
use app\models\blocks\UniqueSellingBlock;
use app\models\blocks\MapBlock;
use app\models\blocks\ContactFormBlock;
use app\models\blocks\ProductBlock;


/**
 * This is the model class for table "page_blocks".
 *
 * @property int $id
 * @property int $page_id
 * @property string $table_name
 * @property int $sorting
 * @property int $field_id
 */
class PageBlocks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_blocks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'sorting', 'field_id'], 'integer'],
            [['table_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'table_name' => 'Table Name',
            'sorting' => 'Sorting',
            'field_id' => 'Field ID',
        ];
    }

    //Получить название блока для меню.
    public function getBlocksField($blocks)
    {
        $items = [];
        foreach ($blocks as $value) {
            
            $name = '';
            $visible = 0;
            switch ($value->table_name) {
                case 'text_block': {
                    $field = TextBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'link_block': {
                    $field = LinkBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'html_block': {
                    $field = HtmlBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'avatar_block': {
                    $field = AvatarBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'social_block': {
                    $field = SocialBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'video_block': {
                    $field = VideoBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'messenger_block': {
                    $field = MessengerBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'question_block': {
                    $field = QuestionBlock::find()->where([ 'number' => $value->field_id ])->one();
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'carusel_block': {
                    $field = CaruselBlock::find()->where([ 'number' => $value->field_id ])->one();
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'advantage_block': {
                    $field = AdvantageBlock::find()->where([ 'number' => $value->field_id ])->one();
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'unique_selling_block': {
                    $field = UniqueSellingBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'map_block': {
                    $field = MapBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'contact_form_block': {
                    $field = ContactFormBlock::findOne($value->field_id);
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                case 'page_block': {
                    $field = UsersPage::findOne($value->field_id)->name;
                    $name = ($field == null ? '' : $field );
                }
                case 'product_block': {
                    $field = ProductBlock::find()->where([ 'number' => $value->field_id ])->one();
                    $visible = $field->visible_in_menu;
                    $name = ($field->name_in_menu == null ? '' : $field->name_in_menu );
                }
                default: return "Неизвестно";
            }
                    
            if($visible) {
                $items [] = [
                    'label' => $name, 
                    'url' => '#'.$value->table_name,
                ];
            }                    
        }
        
        return $items;
    }
}
