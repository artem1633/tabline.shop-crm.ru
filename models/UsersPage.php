<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\blocks\TextBlock;
use app\models\blocks\LinkBlock;
use app\models\blocks\SeparatorBlock;
use app\models\blocks\HtmlBlock;
use app\models\blocks\AvatarBlock;
use app\models\blocks\SocialBlock;
use app\models\blocks\VideoBlock;
use app\models\blocks\MessengerBlock;
use app\models\blocks\AdvantageBlock;
use app\models\blocks\CaruselBlock;
use app\models\blocks\ContactFormBlock;
use app\models\blocks\MapBlock;
use app\models\blocks\MapMarks;
use app\models\blocks\UniqueSellingBlock;
use app\models\blocks\QuestionBlock;
use app\models\blocks\ProductBlock;
use app\models\additional\HistoryPages;
use app\models\Directory;
use app\models\GoodsCatalog;
use app\models\additional\ProductOptions;

/**
 * This is the model class for table "users_page".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $link_name
 *
 * @property TextBlock[] $textBlocks
 * @property Users $user
 */
class UsersPage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $page_number;
    public $file;
    public static function tableName()
    {
        return 'users_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'active', 'is_main', 'page_number', 'type'], 'integer'],
            [['link_name'], 'unique'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['description'], 'string'],
            [['name', 'link_name'], 'required'],
            [['name', 'link_name', 'image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Наименование',
            'link_name' => 'Ссылка',
            'active' => 'Актив',
            'is_main' => 'Главная страница',
            'page_number' => 'Страница',
            'type' => 'Тип страниц',
            'description' => 'Описание',
            'image' => 'Фото',
            'file' => 'Изображение шаблона',

        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $history = new HistoryPages();
            $history->user_id = $this->user_id;
            $history->date = date('Y-m-d');
            $history->page = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->link_name;
            $history->status = 'create';
            $history->save();
        }else{
            $history = new HistoryPages();
            $history->user_id = $this->user_id;
            $history->date = date('Y-m-d');
            $history->page = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->link_name;
            $history->status = 'update';
            $history->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        //----------------------------------------Delete Text Block-----------------------------------------------------//
        $deleteTextBlocks = PageBlocks::find()->where(['table_name' => 'text_block', 'page_id' => $this->id])->all();
        foreach ($deleteTextBlocks as $block) {
            $textBlock = TextBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //------------------------------------------Delete Link Block-----------------------------------------------------//
        $deleteLinkBlocks = PageBlocks::find()->where(['table_name' => 'link_block', 'page_id' => $this->id])->all();
        foreach ($deleteLinkBlocks as $block) {
            $linkBlock = LinkBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //-----------------------------------------Delete Separator Block------------------------------------------------//
        $deleteSeparatorBlocks = PageBlocks::find()->where(['table_name' => 'separator_block', 'page_id' => $this->id])->all();
        foreach ($deleteSeparatorBlocks as $block) {
            $separatorBlock = SeparatorBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //-----------------------------------------Delete Html Block----------------------------------------------------//
        $deleteHtmlBlocks = PageBlocks::find()->where(['table_name' => 'html_block', 'page_id' => $this->id])->all();
        foreach ($deleteHtmlBlocks as $block) {
            $htmlBlock = HtmlBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //-----------------------------------------Delete Avatar Block-------------------------------------------------//
        $deleteAvatarBlocks = PageBlocks::find()->where(['table_name' => 'avatar_block', 'page_id' => $this->id])->all();
        foreach ($deleteAvatarBlocks as $block) {
            $avatarBlock = AvatarBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //------------------------------------------Delete Social Block------------------------------------------------//
        $deleteSocialBlocks = PageBlocks::find()->where(['table_name' => 'social_block', 'page_id' => $this->id])->all();
        foreach ($deleteSocialBlocks as $block) {
            $socialBlock = SocialBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //------------------------------------------Delete Video Block-------------------------------------------------//
        $deleteVideoBlocks = PageBlocks::find()->where(['table_name' => 'video_block', 'page_id' => $this->id])->all();
        foreach ($deleteVideoBlocks as $block) {
            $videoBlock = VideoBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //-----------------------------------------Delete Messenger Block----------------------------------------------------//
        $deleteMessengerBlocks = PageBlocks::find()->where(['table_name' => 'messenger_block', 'page_id' => $this->id])->all();
        foreach ($deleteMessengerBlocks as $block) {
            $messengerBlock = MessengerBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //-----------------------------------------Delete Advantage Block---------------------------------------------------//
        $deleteAdvantageBlocks = PageBlocks::find()->where(['table_name' => 'advantage_block', 'page_id' => $this->id])->all();
        foreach ($deleteAdvantageBlocks as $block) {
            $advantageBlock = AdvantageBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($advantageBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }
        //---------------------------------------------Delete Carusel Block---------------------------------------------------//
        $deleteCaruselBlocks = PageBlocks::find()->where(['table_name' => 'carusel_block', 'page_id' => $this->id])->all();
        foreach ($deleteCaruselBlocks as $block) {
            $caruselBlock = CaruselBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($caruselBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }
        //---------------------------------------------Delete Contact Form Block-------------------------------------------//
        $deleteContactBlocks = PageBlocks::find()->where(['table_name' => 'contact_form_block', 'page_id' => $this->id])->all();
        foreach ($deleteContactBlocks as $block) {
            $contactFormBlock = ContactFormBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //---------------------------------------------Delete Map Form Block--------------------------------------------------//
        $deleteMapBlocks = PageBlocks::find()->where(['table_name' => 'map_block', 'page_id' => $this->id])->all();
        foreach ($deleteMapBlocks as $block) {
            $mapMarks = MapMarks::find()->where(['map_id' => $block->field_id])->all();
            foreach ($mapMarks as $value) $value->delete();
            $mapBlock = MapBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //---------------------------------------------Delete Unique Selling Block--------------------------------------------//
        $deleteUniqueBlocks = PageBlocks::find()->where(['table_name' => 'unique_selling_block', 'page_id' => $this->id])->all();
        foreach ($deleteUniqueBlocks as $block) {
            $uniqueBlock = UniqueSellingBlock::findOne($block->field_id)->delete();
            $block->delete();
        }
        //---------------------------------------------Delete Question Block------------------------------------------------//
        $deleteQuestionBlocks = PageBlocks::find()->where(['table_name' => 'question_block', 'page_id' => $this->id])->all();
        foreach ($deleteQuestionBlocks as $block) {
            $questionBlock = QuestionBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($questionBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }
        //---------------------------------------------Delete Product Block------------------------------------------------//
        $deleteProductBlocks = PageBlocks::find()->where(['table_name' => 'product_block', 'page_id' => $this->id])->all();
        foreach ($deleteProductBlocks as $block) {
            $productBlock = ProductBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($productBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }

        return parent::beforeDelete();
    }


    public function afterDelete() 
    {
        parent::afterDelete();
        //Yii::$app->session->setFlash('success', 'Запись удалено'. $this->id);
        $history = new HistoryPages();
        $history->user_id = $this->user_id;
        $history->date = date('Y-m-d');
        $history->page = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->link_name;
        $history->status = 'delete';
        $history->save();

        if($this->active) {
            $page = UsersPage::find()->where(['user_id' => $this->user_id, 'is_main' => 1])->one();
            if($page != null) {
                $page->active = 1;
                $page->save();
            }
        }

        $users_page = UsersPage::find()->where(['user_id' => $this->user_id])->all();
        foreach ($users_page as $page) {
            $page_block = PageBlocks::find()->where(['table_name' => 'page_block', 'page_id' => $page->id, 'field_id' => $this->id ])->one();
            if($page_block != null) {
                $page_block->delete();
            }            
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatarBlocks()
    {
        return $this->hasMany(AvatarBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextBlocks()
    {
        return $this->hasMany(TextBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkBlocks()
    {
        return $this->hasMany(LinkBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeparatorBlocks()
    {
        return $this->hasMany(SeparatorBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHtmlBlocks()
    {
        return $this->hasMany(HtmlBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialBlocks()
    {
        return $this->hasMany(SocialBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionBlocks()
    {
        return $this->hasMany(QuestionBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaruselBlocks()
    {
        return $this->hasMany(CaruselBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniqueSellingBlocks()
    {
        return $this->hasMany(UniqueSellingBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvantageBlocks()
    {
        return $this->hasMany(AdvantageBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapBlocks()
    {
        return $this->hasMany(MapBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessengerBlocks()
    {
        return $this->hasMany(MessengerBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactFormBlocks()
    {
        return $this->hasMany(ContactFormBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBlocks()
    {
        return $this->hasMany(\app\models\blocks\ProductBlock::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideoBlocks()
    {
        return $this->hasMany(VideoBlock::className(), ['page_id' => 'id']);
    }

    //Получить порядковые номера блоков.
    public function getSortNumber($page_id)
    {
        $number = 0;
        $page_block = PageBlocks::find()->where(['page_id' => $page_id])->orderBy([ 'sorting' => SORT_DESC])->one();
        if($page_block != null) $number = $page_block->sorting;
        $number++;
        return $number;
    }

    //Список страниц ползователя
    public function getPageList($user_id)
    {
        $pages = UsersPage::find()->where(['!=', 'is_main', 1])->andWhere(['user_id' => $user_id])->all();
        return ArrayHelper::map( $pages , 'id', 'name');
    }

    //Получить активные колонки
    public function getItemValues()
    {
        $name = '
            <div class="grid-item" style="text-align:center; font-size:16px;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/users-page/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/users-page/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br><b>' .$this->name . '</b>' .
            '</div>';

        return $name;
    }

    //Загрузка блок на страницу.
    public function setValuesToPageBlocks($page_id, $table_name, $sort, $field_id)
    {
        $page_block = new PageBlocks();
        $page_block->page_id = $page_id;
        $page_block->table_name = $table_name;
        $page_block->sorting = $sort;
        $page_block->field_id = $field_id;
        $page_block->save();
    }

    public function getColumnsTemplate($align, $size, $text)
    {
        return '<div class="grid-item text-danger" style="font-weight:bold; text-align:'.$align.
                    '; height:20px; font-size:'.$size.';">'. $text . '</div>';
    }

    //Загрузка шаблона страницы.
    public function setTemplateBlocks($template_id)
    {
        $template = UsersPage::findOne($template_id);
        $user_id = Yii::$app->user->identity->id;

        //-----------------------Text Block-------Delete and Insert values to Text Block-----------------//
        $deleteTextBlocks = PageBlocks::find()->where(['table_name' => 'text_block', 'page_id' => $this->id])->all();
        foreach ($deleteTextBlocks as $block) {
            $textBlock = TextBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $textBlocks = PageBlocks::find()->where(['table_name' => 'text_block', 'page_id' => $template->id])->all();
        foreach ($textBlocks as $value) {
            $textBlock = TextBlock::findOne($value->field_id);
            $newTextBlock = new TextBlock();
            $newTextBlock->attributes = $textBlock->attributes;
            $newTextBlock->page_id = $this->id;
            $newTextBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'text_block', $newTextBlock->page->getSortNumber($this->id), $newTextBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Link Block-------Delete and Insert values to Link Block-----------------//
        $deleteLinkBlocks = PageBlocks::find()->where(['table_name' => 'link_block', 'page_id' => $this->id])->all();
        foreach ($deleteLinkBlocks as $block) {
            $linkBlock = LinkBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $linkBlocks = PageBlocks::find()->where(['table_name' => 'link_block', 'page_id' => $template->id])->all();
        foreach ($linkBlocks as $value) {
            $linkBlock = LinkBlock::findOne($value->field_id);
            $newLinkBlock = new LinkBlock();
            $newLinkBlock->attributes = $linkBlock->attributes;
            $newLinkBlock->page_id = $this->id;
            $newLinkBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'link_block', $newLinkBlock->page->getSortNumber($this->id), $newLinkBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Separator Block-------Delete and Insert values to Separator Block-----------------//
        $deleteSeparatorBlocks = PageBlocks::find()->where(['table_name' => 'separator_block', 'page_id' => $this->id])->all();
        foreach ($deleteSeparatorBlocks as $block) {
            $separatorBlock = SeparatorBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $separatorBlocks = PageBlocks::find()->where(['table_name' => 'separator_block', 'page_id' => $template->id])->all();
        foreach ($separatorBlocks as $value) {
            $separatorBlock = SeparatorBlock::findOne($value->field_id);
            $newSeparatorBlock = new SeparatorBlock();
            $newSeparatorBlock->attributes = $separatorBlock->attributes;
            $newSeparatorBlock->page_id = $this->id;
            $newSeparatorBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'separator_block', $newSeparatorBlock->page->getSortNumber($this->id), $newSeparatorBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Html Block-------Delete and Insert values to Html Block-----------------//
        $deleteHtmlBlocks = PageBlocks::find()->where(['table_name' => 'html_block', 'page_id' => $this->id])->all();
        foreach ($deleteHtmlBlocks as $block) {
            $htmlBlock = HtmlBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $separatorBlocks = PageBlocks::find()->where(['table_name' => 'html_block', 'page_id' => $template->id])->all();
        foreach ($separatorBlocks as $value) {
            $htmlBlock = HtmlBlock::findOne($value->field_id);
            $newHtmlBlock = new HtmlBlock();
            $newHtmlBlock->attributes = $htmlBlock->attributes;
            $newHtmlBlock->page_id = $this->id;
            $newHtmlBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'html_block', $newHtmlBlock->page->getSortNumber($this->id), $newHtmlBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Avatar Block-------Delete and Insert values to Avatar Block-----------------//
        $deleteAvatarBlocks = PageBlocks::find()->where(['table_name' => 'avatar_block', 'page_id' => $this->id])->all();
        foreach ($deleteAvatarBlocks as $block) {
            $avatarBlock = AvatarBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $avatarBlocks = PageBlocks::find()->where(['table_name' => 'avatar_block', 'page_id' => $template->id])->all();
        foreach ($avatarBlocks as $value) {
            $avatarBlock = AvatarBlock::findOne($value->field_id);
            $newAvatarBlock = new AvatarBlock();
            $newAvatarBlock->attributes = $avatarBlock->attributes;
            $newAvatarBlock->page_id = $this->id;
            $newAvatarBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'avatar_block', $newAvatarBlock->page->getSortNumber($this->id), $newAvatarBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Social Block-------Delete and Insert values to Social Block-----------------//
        $deleteSocialBlocks = PageBlocks::find()->where(['table_name' => 'social_block', 'page_id' => $this->id])->all();
        foreach ($deleteSocialBlocks as $block) {
            $socialBlock = SocialBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $socialBlocks = PageBlocks::find()->where(['table_name' => 'social_block', 'page_id' => $template->id])->all();
        foreach ($socialBlocks as $value) {
            $socialBlock = SocialBlock::findOne($value->field_id);
            $newSocialBlock = new SocialBlock();
            $newSocialBlock->attributes = $socialBlock->attributes;
            $newSocialBlock->page_id = $this->id;
            $newSocialBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'social_block', $newSocialBlock->page->getSortNumber($this->id), $newSocialBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Video Block-------Delete and Insert values to Video Block-----------------//
        $deleteVideoBlocks = PageBlocks::find()->where(['table_name' => 'video_block', 'page_id' => $this->id])->all();
        foreach ($deleteVideoBlocks as $block) {
            $videoBlock = VideoBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $videoBlocks = PageBlocks::find()->where(['table_name' => 'video_block', 'page_id' => $template->id])->all();
        foreach ($videoBlocks as $value) {
            $videoBlock = VideoBlock::findOne($value->field_id);
            $newVideoBlock = new VideoBlock();
            $newVideoBlock->attributes = $videoBlock->attributes;
            $newVideoBlock->page_id = $this->id;
            $newVideoBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'video_block', $newVideoBlock->page->getSortNumber($this->id), $newVideoBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Messenger Block-------Delete and Insert values to Messenger Block-----------------//
        $deleteMessengerBlocks = PageBlocks::find()->where(['table_name' => 'messenger_block', 'page_id' => $this->id])->all();
        foreach ($deleteMessengerBlocks as $block) {
            $messengerBlock = MessengerBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $messengerBlocks = PageBlocks::find()->where(['table_name' => 'messenger_block', 'page_id' => $template->id])->all();
        foreach ($messengerBlocks as $value) {
            $messengerBlock = MessengerBlock::findOne($value->field_id);
            $newMessengerBlock = new MessengerBlock();
            $newMessengerBlock->attributes = $messengerBlock->attributes;
            $newMessengerBlock->page_id = $this->id;
            $newMessengerBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'messenger_block', $newMessengerBlock->page->getSortNumber($this->id), $newMessengerBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Advantage Block-------Delete and Insert values to Advantage Block-----------------//
        $deleteAdvantageBlocks = PageBlocks::find()->where(['table_name' => 'advantage_block', 'page_id' => $this->id])->all();
        foreach ($deleteAdvantageBlocks as $block) {
            $advantageBlock = AdvantageBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($advantageBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }

        $advantageBlocks = PageBlocks::find()->where(['table_name' => 'advantage_block', 'page_id' => $template->id])->all();
        $number = AdvantageBlock::getLastId();
        $targetDir = Directory::createAdvantageDirectory($number);

        foreach ($advantageBlocks as $value) {
            $advantageBlock = AdvantageBlock::find()->where(['number' => $value->field_id])->all();
            foreach ($advantageBlock as $value) {
                $newAdvantageBlock = new AdvantageBlock();
                $newAdvantageBlock->attributes = $value->attributes;
                $newAdvantageBlock->page_id = $this->id;
                $newAdvantageBlock->number = $number;
                $newAdvantageBlock->save();

                $file = 'images/advantage-block/' . $value->number . '/' . $value->image;
                $newfile = 'images/advantage-block/' . $number . '/' . $newAdvantageBlock->image;
                 
                if (!copy($file, $newfile)) { }               
            }
            $this->setValuesToPageBlocks($this->id, 'advantage_block', $newAdvantageBlock->page->getSortNumber($this->id), $number );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Carusel Block-------Delete and Insert values to Carusel Block-----------------//
        $deleteCaruselBlocks = PageBlocks::find()->where(['table_name' => 'carusel_block', 'page_id' => $this->id])->all();
        foreach ($deleteCaruselBlocks as $block) {
            $caruselBlock = CaruselBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($caruselBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }

        $caruselBlocks = PageBlocks::find()->where(['table_name' => 'carusel_block', 'page_id' => $template->id])->all();
        $number = CaruselBlock::getLastId();
        $targetDir = Directory::createCaruselDirectory($number);

        foreach ($caruselBlocks as $value) {
            $caruselBlock = CaruselBlock::find()->where(['number' => $value->field_id])->all();
            foreach ($caruselBlock as $value) {
                $newCaruselBlock = new CaruselBlock();
                $newCaruselBlock->attributes = $value->attributes;
                $newCaruselBlock->page_id = $this->id;
                $newCaruselBlock->number = $number;
                $newCaruselBlock->save();

                $file = 'images/carusel-block/' . $value->number . '/' . $value->image;
                $newfile = 'images/carusel-block/' . $number . '/' . $newCaruselBlock->image;
                 
                if (!copy($file, $newfile)) { }               
            }
            $this->setValuesToPageBlocks($this->id, 'carusel_block', $newCaruselBlock->page->getSortNumber($this->id), $number );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Contact Form Block-------Delete and Insert values to Contact Form Block-----------------//
        $deleteContactBlocks = PageBlocks::find()->where(['table_name' => 'contact_form_block', 'page_id' => $this->id])->all();
        foreach ($deleteContactBlocks as $block) {
            $contactFormBlock = ContactFormBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $contactFormBlocks = PageBlocks::find()->where(['table_name' => 'contact_form_block', 'page_id' => $template->id])->all();
        foreach ($contactFormBlocks as $value) {
            $contactFormBlock = ContactFormBlock::findOne($value->field_id);
            $newContactBlock = new ContactFormBlock();
            $newContactBlock->attributes = $contactFormBlock->attributes;
            $newContactBlock->page_id = $this->id;
            $newContactBlock->save();
            
            $this->setValuesToPageBlocks($this->id, 'contact_form_block', UsersPage::getSortNumber($this->id), $newContactBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Map Form Block-------Delete and Insert values to Map Form Block-----------------//
        $deleteMapBlocks = PageBlocks::find()->where(['table_name' => 'map_block', 'page_id' => $this->id])->all();
        foreach ($deleteMapBlocks as $block) {
            $mapMarks = MapMarks::find()->where(['map_id' => $block->field_id])->all();
            foreach ($mapMarks as $value) $value->delete();
            $mapBlock = MapBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $mapBlocks = PageBlocks::find()->where(['table_name' => 'map_block', 'page_id' => $template->id])->all();
        foreach ($mapBlocks as $value) {
            $mapBlock = MapBlock::findOne($value->field_id);
            $newMapBlock = new MapBlock();
            $newMapBlock->attributes = $mapBlock->attributes;
            $newMapBlock->page_id = $this->id;
            $newMapBlock->save();

            $mapMarks = MapMarks::find()->where(['map_id' => $value->field_id])->all();
            foreach ($mapMarks as $value) {
                $newMark = new MapMarks();
                $newMark->attributes = $value->attributes;
                $newMark->map_id = $newMapBlock->id;
                $newMark->save();
            }
            
            $this->setValuesToPageBlocks($this->id, 'map_block', $newMapBlock->page->getSortNumber($this->id), $newMapBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Unique Selling Block-------Delete and Insert values to Unique Selling Block-----------------//
        $deleteUniqueBlocks = PageBlocks::find()->where(['table_name' => 'unique_selling_block', 'page_id' => $this->id])->all();
        foreach ($deleteUniqueBlocks as $block) {
            $uniqueBlock = UniqueSellingBlock::findOne($block->field_id)->delete();
            $block->delete();
        }

        $uniqueBlocks = PageBlocks::find()->where(['table_name' => 'unique_selling_block', 'page_id' => $template->id])->all();
        foreach ($uniqueBlocks as $value) {
            $uniqueBlock = UniqueSellingBlock::findOne($value->field_id);
            $newUniqueBlock = new UniqueSellingBlock();
            $newUniqueBlock->attributes = $uniqueBlock->attributes;
            $newUniqueBlock->page_id = $this->id;
            $newUniqueBlock->save();
            $extension = $newUniqueBlock->getExtension();
            $newUniqueBlock->image = $newUniqueBlock->id . '.' . $extension;
            $newUniqueBlock->save();

            $file = 'images/unique-selling-block/' . $uniqueBlock->image;
            $newfile = 'images/unique-selling-block/' . $newUniqueBlock->image;
                 
            if (!copy($file, $newfile)) { }
            
            $this->setValuesToPageBlocks($this->id, 'unique_selling_block', $newUniqueBlock->page->getSortNumber($this->id), $newUniqueBlock->id );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //-----------------------Question Block-------Delete and Insert values to Question Block-----------------//
        $deleteQuestionBlocks = PageBlocks::find()->where(['table_name' => 'question_block', 'page_id' => $this->id])->all();
        foreach ($deleteQuestionBlocks as $block) {
            $questionBlock = QuestionBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($questionBlock as $value) {
                $value->delete();
            }
            $block->delete();
        }

        $questionBlocks = PageBlocks::find()->where(['table_name' => 'question_block', 'page_id' => $template->id])->all();
        $number = QuestionBlock::getLastId();

        foreach ($questionBlocks as $value) {
            $questionBlock = QuestionBlock::find()->where(['number' => $value->field_id])->all();
            foreach ($questionBlock as $value) {
                $newQuestionBlock = new QuestionBlock();
                $newQuestionBlock->attributes = $value->attributes;
                $newQuestionBlock->page_id = $this->id;
                $newQuestionBlock->number = $number;
                $newQuestionBlock->save();          
            }
            $this->setValuesToPageBlocks($this->id, 'question_block', $newQuestionBlock->page->getSortNumber($this->id), $number );
        }
        //----------------------------------------------------------------------------------------------//
        //
        //---------------------Product Block-------Delete and Insert values to Product Block--------------------//
        /*$deleteProductBlocks = PageBlocks::find()->where(['table_name' => 'product_block', 'page_id' => $this->id])->all();
        foreach ($deleteProductBlocks as $block) 
        {
            $productBlock = ProductBlock::find()->where(['number' => $block->field_id])->all();
            foreach ($productBlock as $value) 
            {
                $value->delete();
            }
            $block->delete();
        }

        $productBlocks = PageBlocks::find()->where(['table_name' => 'product_block', 'page_id' => $template->id])->all();
        $number = ProductBlock::getLastId();

        foreach ($productBlocks as $value) 
        {
            $newProductBlock = ProductBlock::find()->where(['number' => $value->field_id])->all();
            foreach ($newProductBlock as $value) 
            {
                //goods catalog
                $goods_catalog = GoodsCatalog::find()->where(['id' => $value->product->catalog_id ])->one();
                $catalog = GoodsCatalog::find()->where(['user_id' => $user_id, 'name' => $goods_catalog->name, 'hide' => $goods_catalog->hide])->one();

                if($catalog == null)
                {
                    $catalog = new GoodsCatalog();
                    $catalog->user_id = $user_id;
                    $catalog->name = $goods_catalog->name;
                    $catalog->hide = $goods_catalog->hide;
                    $catalog->save();
                }

                //new product
                $product = Products::find()->where([
                    'user_id' => $user_id,
                    'name' => $value->product->name,
                    'description' => $value->product->description,
                    'price' => $value->product->price,
                    'last_price' => $value->product->last_price,
                    'catalog_id' => $value->product->catalog_id,
                    'weight' => $value->product->weight,
                    'delivery' => $value->product->delivery,
                    'view_in_catalog' => $value->product->view_in_catalog,
                    'status' => $value->product->status,
                ])->one();

                if($product == null)
                {
                    $product = new Products();
                    $product->attributes = $value->product->attributes;
                    $product->catalog_id = $catalog->id;
                    $product->user_id = $user_id;

                    $data = json_decode($value->product->photos);
                    $result = [];
                    foreach ($data as $value) {
                        $result [] = [
                            'name' => $value->name,
                            'size' => $value->size,
                            'type' => $value->type,
                            'path' => $value->path,
                        ];
                    }
                    $product->photos = json_encode($result);
                    $product->save();
                }

                //new product options
                $options = ProductOptions::find()->where(['product_id' => $value->product_id ])->all();
                foreach ($options as $value) {
                    $option = new ProductOptions();
                    $option->product_id = $product->id;
                    $option->name = $value->name;
                    $option->price = $value->price;
                    $option->save();
                }

                $newProductBlock = new ProductBlock();
                $newProductBlock->attributes = $value->attributes;
                $newProductBlock->page_id = $this->id;
                $newProductBlock->product_id = $product->id;
                $newProductBlock->number = $number;
                $newProductBlock->save();          
            }
            $this->setValuesToPageBlocks($this->id, 'product_block', $newProductBlock->page->getSortNumber($this->id), $number );
        }*/
        //------------------------------------------------------------------------------------------------------------------//
    }
}
