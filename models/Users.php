<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use app\models\Products;
use app\models\Applications;
use app\models\additional\Common;
use app\models\additional\Columns;
use app\models\additional\Tariffs;
use app\models\additional\Permissions;
use app\models\additional\LegalInformation;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property int $type
 * @property string $avatar
 * @property string $telephone
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_password;
    public $file;
    public $social;
    public $page;

    const SUPER_ADMINISTRATOR = '0';
    const ADMINISTRATOR = '1';
    const MANAGER = '2';
    const USER = '3';

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'type'], 'required'],
            [['type', 'relation', 'tariff_id', 'paternity_user_code'], 'integer'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['login'], 'email'],
            [['total_payment_amount'], 'number'],
            [['registry_date', 'last_payment_date', 'end_access'], 'safe'],
            [['instagram_user_id', 'facebook_user_id', 'vk_user_id', 'google_account_user_id'], 'string', 'max' => 500],
            [['fio', 'login', 'password', 'telephone', 'new_password', 'partner_code', 'promo_code'], 'string', 'max' => 255],
            [['avatar'], 'string'],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariffs::className(), 'targetAttribute' => ['tariff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Тип',
            'avatar' => 'Avatar',
            'telephone' => 'Телефон',
            'new_password' => 'Новый пароль',
            'instagram_user_id' => 'instagram_user_id',
            'facebook_user_id' => 'facebook_user_id',
            'vk_user_id' => 'vk_user_id',
            'google_account_user_id' => 'google_account_user_id',
            'total_payment_amount' => 'Общая сумма платежей',
            'registry_date' => 'Дата регистрации',
            'last_payment_date' => 'Дата последнего платежа',
            'file' => 'Аватар',
            'end_access' => 'Срок действия тарифного плана',
            'relation' => 'Аккаунты',
            'tariff_id' => 'Тарифный план',
            'partner_code' => 'Партнерский код',
            'paternity_user_code' => 'Кол-во акк. в реф. программе',
            'promo_code' => 'Промокод',
            'social' => 'Соц. сети',
            'page' => 'Страницы',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {

            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            $this->registry_date = date('Y-m-d');

            if($this->paternity_user_code == null) {
                $this->tariff_id = Tariffs::find()->where(['key' => 'pro'])->one()->id;
                $this->end_access = date('Y-m-d', strtotime( "+14 days"));
            }else{
                $this->tariff_id = Tariffs::find()->where(['key' => 'pro'])->one()->id;
                $this->end_access = date('Y-m-d', strtotime( "+1 month"));
            }

            $length = 20;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Users::find()->where(['partner_code' => $string])->one() != null );

            $this->partner_code = $string;
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $user_id = $this->id;
            //Yii::$app->session->setFlash('success', 'Запись добавлена');
            $common = Common::find()->where(['user_id' => $user_id])->one();
            if($common == null) Common::setDefaultValues($user_id);

            $products = Columns::find()->where(['user_id' => $user_id, 'table_name' => 'products'])->one();
            if($products == null ) Products::setDefaultValues($user_id);

            $applications = Columns::find()->where(['user_id' => $user_id, 'table_name' => 'applications'])->one();
            if($applications == null ) Applications::setDefaultValues($user_id);

            $legal = LegalInformation::find()->where(['user_id' => $user_id,])->one();
            if($legal == null ) LegalInformation::setDefaultValues($user_id);

            if($this->type == 2) Permissions::setDefaultValues($user_id);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        $user_id = $this->id;
        if ($this->avatar != null && file_exists('avatars/' . $this->avatar)) unlink(Yii::getAlias('avatars/' . $this->avatar));

        $products = Columns::find()->where(['user_id' => $user_id, 'table_name' => 'products'])->all();
        foreach ($products as $column) {
            $column->delete();
        }

        $applications = Columns::find()->where(['user_id' => $user_id, 'table_name' => 'applications'])->all();
        foreach ($applications as $column) {
            $column->delete();
        }

        $common = Common::find()->where(['user_id' => $user_id])->one();
        if($common != null) $common->delete();

        $legal = LegalInformation::find()->where(['user_id' => $user_id])->one();
        if($legal != null) $legal->delete();

        if($this->type == 2) $permission = Permissions::find()->where(['user_id' => $this->id])->one();
        if($permission != null) $permission->delete();

        return parent::beforeDelete();
    }

    //Получить список типов пользователя.
    public function getType()
    {
        return [
            self::ADMINISTRATOR => 'Администратор',
            self::MANAGER => 'Менеджер',
            self::USER => 'Пользователь',
        ];
    }

    //Получить описание типов пользователя.
    public function getTypeDescription()
    {
        switch ($this->type) {
            case 0: return "Супер администратор";
            case 1: return "Администратор";
            case 2: return "Менеджер";
            case 3: return "Пользователь";
            default: return "Неизвестно";
        }
    }

    //Список тарифов.
    public function getTariffList()
    {
        $tariffs = Tariffs::find()->all();
        return ArrayHelper::map( $tariffs , 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersPages()
    {
        return $this->hasMany(UsersPage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCatalogs()
    {
        return $this->hasMany(GoodsCatalog::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(Permissions::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contacts::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColumns()
    {
        return $this->hasMany(Columns::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommons()
    {
        return $this->hasMany(Common::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAccessesFrom()
    {
        return $this->hasMany(UsersAccess::className(), ['user_from' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAccessesTo()
    {
        return $this->hasMany(UsersAccess::className(), ['user_to' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariffs::className(), ['id' => 'tariff_id']);
    }

}
