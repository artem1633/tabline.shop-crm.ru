<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "html_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $text
 *
 * @property UsersPage $page
 */
class HtmlBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'html_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['text'], 'string'],
            [['text'], 'required'],
            [['title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'text' => 'Текст',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить данные блока
    public function getItemValues()
    {
         $name = '
            <div class="grid-item" style="text-align:center" >
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/html-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/html-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . $this->text . 
            '</div>';

        return $name;
    }
}
