<?php

namespace app\models\blocks;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\VideoBlock;

/**
 * VideoBlockSearch represents the model behind the search form about `app\models\blocks\VideoBlock`.
 */
class VideoBlockSearch extends VideoBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'show_elements', 'visible', 'visible_in_menu'], 'integer'],
            [['link', 'description', 'title_text', 'name_in_menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VideoBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'show_elements' => $this->show_elements,
            'visible' => $this->visible,
            'visible_in_menu' => $this->visible_in_menu,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'name_in_menu', $this->name_in_menu])
            ->andFilterWhere(['like', 'title_text', $this->title_text])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
