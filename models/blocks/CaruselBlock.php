<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\additional\Clicks;

/**
 * This is the model class for table "carusel_block".
 *
 * @property int $id
 * @property int $page_id
 * @property int $number
 * @property string $size_slide
 * @property int $time
 * @property string $image
 * @property string $title
 * @property string $text
 *
 * @property UsersPage $page
 */
class CaruselBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carusel_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'number', 'time', 'visible', 'visible_in_menu'], 'integer'],
            [['text'], 'string'],
            [['size_slide', 'image', 'title', 'title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'number' => 'Номер',
            'size_slide' => 'Размера слайдера',
            'time' => 'Автоматическая смена слайдов',
            'image' => 'Слайд',
            'title' => 'Заголовок',
            'text' => 'Описания',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    public function beforeDelete()
    {
        if (file_exists('images/carusel-block/' . $this->number . '/' . $this->image)) {
            unlink(Yii::getAlias('images/carusel-block/' . $this->number . '/' . $this->image));
        }

        return parent::beforeDelete();
    }

    public function afterDelete() 
    {
        parent::afterDelete();
        $clicks = Clicks::find()->where(['field_id' => $this->id, 'table_name' => 'carusel_block'])->all();
        foreach ($clicks as $value) {
            $value->delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить список размера
    public function getSlideSizeList()
    {
        return [
            'square' => 'Квадрат 620х620px',
            'extruded_rectangle' => 'Средний (95х95px)',
            'rectangle' => 'Большой (125х125px)',
            'vertical_rectangle' => 'Очень большой (150х150px)',
        ];
    }

    //Получить описание размера
    public function getSizeDescription()
    {
        switch ($this->size_slide) {
            case 'square': return "620";
            case 'extruded_rectangle': return "310";
            case 'rectangle': return "438";
            case 'vertical_rectangle': return "877";
            default: return "Неизвестно";
        }
    }

    //Получить последнюю ид из БД.
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'carusel_block'")
            ->one();
        if($res){
            return $res["AUTO_INCREMENT"];
        }
    }

    //Получить данные блока
    public function getItemValues($slide)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/carusel-block/remove', 'number'=> $this->number  ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/carusel-block/update', 'number'=>$this->number, 'page_id'=>$this->page_id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br>' . $slide. 
            '</div>';

        return $name;
    }
}
