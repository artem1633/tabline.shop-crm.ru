<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "unique_selling_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property string $button_text
 * @property string $link
 *
 * @property UsersPage $page
 */
class UniqueSellingBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unique_selling_block';
    }
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['text'], 'string'],
            //[['title', 'text', 'button_text', 'link'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['title', 'image', 'link', 'title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['button_text'], 'string', 'max' => 50],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'image' => 'Фоновое изображение',
            'button_text' => 'Надпись на кнопке',
            'link' => 'Ссылка на кнопке',
            'file' => 'Фоновое изображение',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    public function beforeDelete()
    {
        if (file_exists('images/unique-selling-block/'.$this->image)) {
            unlink(Yii::getAlias('images/unique-selling-block/'. $this->image));
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить данные блока
    public function getItemValues($page)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/unique-selling-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/unique-selling-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . $page .  
            '</div>';

        return $name;
    }

    //Получить расширение картинок
    public function getExtension()
    {
        $image = $this->image;
        $strlen = strlen( $image );
        $extension = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) {
            $char = substr( $image, $i, 1 );             
            if($char == '.') { 
                $extension = substr($image, $i+1, $strlen - $i - 1 ); 
                break;
            }
        }
        return $extension;
    }
}
