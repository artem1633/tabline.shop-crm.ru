<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "product_block".
 *
 * @property int $id
 * @property int $page_id
 * @property int $number
 * @property string $view
 * @property int $product_id
 * @property int $contact_form_id
 * @property int $checkbox
 * @property int $foto_visible
 * @property int $name_visible
 * @property int $description_visible
 * @property int $price_visible
 * @property int $contact_form_visible
 * @property int $button_visible
 * @property int $visible
 * @property string $title_text
 * @property int $visible_in_menu
 * @property string $name_in_menu
 *
 * @property UsersPage $page
 * @property Products $product
 */
class ProductBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'number', 'product_id', 'contact_form_id', 'checkbox', 'foto_visible', 'name_visible', 'description_visible', 'price_visible', 'contact_form_visible', 'button_visible', 'visible', 'visible_in_menu'], 'integer'],
            [['view'], 'string', 'max' => 20],
            [['title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'number' => 'Номер',
            'view' => 'Вид вывода товаров',
            'product_id' => 'Товар',
            'contact_form_id' => 'Контакт форма',
            'checkbox' => '', //'Отправляя эту форму, я принимаю соглашение об обработке пользовательских данных',
            'foto_visible' => 'Показать фото',
            'name_visible' => 'Показать наименование товара',
            'description_visible' => 'Показать описание',
            'price_visible' => 'Показать цена',
            'contact_form_visible' => 'Показать форма контактов',
            'button_visible' => 'Показать кнопку',
            'visible' => 'Блок виден всем посетителям',
            'title_text' => 'Заголовок',
            'visible_in_menu' => 'Блок виден в меню',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Products::className(), ['id' => 'product_id']);
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'product_block'")
            ->one();
        if($res){
            return $res["AUTO_INCREMENT"];
        }
    }

    //Получить список действии
    public function getViewList()
    {
        return [
            'slider' => 'Слайдер товаров',
            'table' => 'Таблица товаров',
            'list' => 'Выпадающий список товаров',
        ];
    }

    //Получить список продуктов
    public function getProductsList()
    {
        $product = \app\models\Products::find()->where(['user_id' => Yii::$app->user->identity->id, 'status' => 'active'])->all();
        $data = '';
        foreach ($product as $value) {
            if($value->id == $this->product->id) $data .= '<option selected value="'.$value->id.'">'.$value->name.'</option>';
            else $data .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $data;
    }

    //Получить список продуктов
    public function ProductsList($productBlock)
    {
        $data = '<option value="0">Выберите товар</option>';
        foreach ($productBlock as $value) {
            $data .= '<option value="'.$value->product_id.'">'.$value->product->name.'</option>';
        }
        return $data;
    }

    //Получить данные блока
    public function getItemValues($slide)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/product-block/remove', 'number'=> $this->number  ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/product-block/update', 'number'=>$this->number, 'page_id'=>$this->page_id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br>' . $slide . 
            '</div>';

        return $name;
    }
}
