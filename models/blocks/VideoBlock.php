<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\additional\Clicks;

/**
 * This is the model class for table "video_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $link
 * @property string $description
 *
 * @property UsersPage $page
 */
class VideoBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'show_elements', 'visible', 'visible_in_menu'], 'integer'],
            [['description'], 'string'],
            [['link'], 'required'],
            [['title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 500],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'link' => 'Ссылка на видео Youtube или Vimeo',
            'description' => 'Описание',
            'show_elements' => 'Скрыть элементы управления',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    public function afterDelete() 
    {
        parent::afterDelete();
        $clicks = Clicks::find()->where(['field_id' => $this->id, 'table_name' => 'video_block'])->all();
        foreach ($clicks as $value) {
            $value->delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить данные блока
    public function getItemValues($video)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/video-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/video-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br>' . $video .  
            '</div>';

        return $name;
    }
}
