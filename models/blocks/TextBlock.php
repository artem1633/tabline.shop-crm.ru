<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "text_block".
 *
 * @property int $id
 * @property int $page_id
 * @property int $sorting
 * @property string $text_size
 * @property string $alignment
 *
 * @property UsersPage $page
 */
class TextBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['text_size', 'alignment'], 'string', 'max' => 50],
            [['text'], 'string'],
            [['title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'text_size' => 'Размер текста',
            'alignment' => 'Выравнивание',
            'text' => 'Текст',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    //Получить список Размеров текста.
    public function getTextSize()
    {
        return [
            'small' => 'Маленький',
            'middle' => 'Средний',
            'big' => 'Большой',
            'very_big' => 'Очень большой',
        ];
    }

    //Получить описание Размеров текста
    public function getTextSizeDescription()
    {
        switch ($this->text_size) {
            case 'small': return "10px";
            case 'middle': return "12px";
            case 'big': return "16px";
            case 'very_big': return "22px";
            default: return "Неизвестно";
        }
    }

    //Получить список Выравниваний.
    public function getAlignmentSize()
    {
        return [
            'left' => 'По левому краю',
            'right' => 'По правому краю',
            'center' => 'По центру',
        ];
    }

    //Получить описание Выравниваний
    public function getAlignmentSizeDescription()
    {
        if($this->alignment == 'left') return 'left';
        if($this->alignment == 'right') return 'right';
        if($this->alignment == 'center') return 'center';

        switch ($this->alignment) {
            case 'left': return "left";
            case 'right': return "right";
            case 'center': return "center";
            default: return "Неизвестно";
        }
    }

    //Получить данные блока
    public function getItemValues()
    {
        $size = $this->getTextSizeDescription();
        $align = $this->getAlignmentSizeDescription();
        if($this->text == '') {
            $text = '<span style="color:#a94442;">Текст не задано</span>';
        }else{
            $text = $this->text;
        }
        $name = '
            <div class="grid-item" style="text-align:'.$align.'; font-size:'.$size.';">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/text-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/text-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br>' .$text .  
            '</div>';

        return $name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }
}
