<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UsersAccess;

/**
 * UsersAccessSearch represents the model behind the search form about `app\models\UsersAccess`.
 */
class UsersAccessSearch extends UsersAccess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_from', 'user_to', 'access_type'], 'integer'],
            [['view_page', 'update_page', 'view_application', 'update_application', 'view_statics', 'update_settings'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type)
    {
        if($type == 1) $query = UsersAccess::find()->where(['user_from' => Yii::$app->user->identity->id]);
        if($type == 2) $query = UsersAccess::find()->where(['user_to' => Yii::$app->user->identity->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_from' => $this->user_from,
            'user_to' => $this->user_to,
            'access_type' => $this->access_type,
        ]);

        $query->andFilterWhere(['like', 'view_page', $this->view_page])
            ->andFilterWhere(['like', 'update_page', $this->update_page])
            ->andFilterWhere(['like', 'view_application', $this->view_application])
            ->andFilterWhere(['like', 'update_application', $this->update_application])
            ->andFilterWhere(['like', 'view_statics', $this->view_statics])
            ->andFilterWhere(['like', 'update_settings', $this->update_settings]);

        return $dataProvider;
    }
}
