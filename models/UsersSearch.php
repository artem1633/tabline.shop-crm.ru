<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'relation'], 'integer'],
            [['fio', 'login', 'password', 'avatar', 'telephone', 'instagram_user_id', 'facebook_user_id', 'vk_user_id', 'google_account_user_id', 'last_payment_date', 'registry_date', 'total_payment_amount', 'end_access', 'partner_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'instagram_user_id' => $this->instagram_user_id,
            'facebook_user_id' => $this->facebook_user_id,
            'vk_user_id' => $this->vk_user_id,
            'google_account_user_id' => $this->google_account_user_id,
            'last_payment_date' => $this->last_payment_date,
            'registry_date' => $this->registry_date,
            'total_payment_amount' => $this->total_payment_amount,
            'end_access' => $this->end_access,
            'relation' => $this->relation,
            'partner_code' => $this->partner_code,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'telephone', $this->telephone]);

        return $dataProvider;
    }

    public function getList($params,$relation)
    {
        $query = Users::find()->where(['relation' => $relation]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'instagram_user_id' => $this->instagram_user_id,
            'facebook_user_id' => $this->facebook_user_id,
            'vk_user_id' => $this->vk_user_id,
            'google_account_user_id' => $this->google_account_user_id,
            'last_payment_date' => $this->last_payment_date,
            'registry_date' => $this->registry_date,
            'total_payment_amount' => $this->total_payment_amount,
            'end_access' => $this->end_access,
            'relation' => $this->relation,
            'partner_code' => $this->partner_code,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'telephone', $this->telephone]);

        return $dataProvider;
    }
}
