<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `app\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'catalog_id', 'delivery', 'view_in_catalog'], 'integer'],
            [['name', 'description', 'photos', 'status', 'date_cr', 'date_up', 'link'], 'safe'],
            [['price', 'last_price', 'weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$post)
    {
        $query = Products::find()->where(['status' => 'active' ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            //'price' => $this->price,
            'last_price' => $this->last_price,
            //'catalog_id' => $this->catalog_id,
            'weight' => $this->weight,
            'delivery' => $this->delivery,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
            'view_in_catalog' => $this->view_in_catalog,
            'link' => $this->link,
        ]);

        $query->andFilterWhere(['catalog_id' => $post['ProductsSearch']['search_catalog']]);
        $query->andFilterWhere(['name' => $post['ProductsSearch']['search_name']]);
        $query->andFilterWhere(['price' => $post['ProductsSearch']['search_price']]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

    public function searchArchives($params,$post)
    {
        $query = Products::find()->where(['status' => 'archive' ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            //'price' => $this->price,
            'last_price' => $this->last_price,
            //'catalog_id' => $this->catalog_id,
            'weight' => $this->weight,
            'delivery' => $this->delivery,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
            'view_in_catalog' => $this->view_in_catalog,
        ]);

        $query->andFilterWhere(['catalog_id' => $post['ProductsSearch']['search_catalog_archive']]);
        $query->andFilterWhere(['name' => $post['ProductsSearch']['search_name_archive']]);
        $query->andFilterWhere(['price' => $post['ProductsSearch']['search_price_archive']]);
        
        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
