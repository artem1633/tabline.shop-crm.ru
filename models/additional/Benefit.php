<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "benefit".
 *
 * @property int $id
 * @property string $name Название изображения
 * @property string $teg Тег
 * @property string $image Фото
 */
class Benefit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'benefit';
    }

    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teg'], 'string'],
            [['name'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название изображения',
            'teg' => 'Тег',
            'image' => 'Фото',
            'file' => 'Фото',
        ];
    }

    public function beforeDelete()
    {
        if (file_exists('images/benefit/'. $this->image)) {
            unlink(Yii::getAlias('images/benefit/' .  $this->image));
        }
        return parent::beforeDelete();
    }
}
