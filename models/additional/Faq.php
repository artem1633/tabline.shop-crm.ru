<?php

namespace app\models\additional;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string $question Вопрос
 * @property string $answer Ответ
 * @property int $status Статус
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'string'],
            [['status'], 'integer'],
            [['question', 'answer'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'status' => 'Статус',
        ];
    }

    //Получить список статус
    public function getStatusList()
    {
        return [
            0 => 'Не активно',
            1 => 'Активно',
        ];
    }

    //Получить описание статуса
    public function getStatusDescription()
    {
        switch ($this->status) {
            case 0: return "Не активно";
            case 1: return "Активно";
            default: return "Неизвестно";
        }
    }
}
