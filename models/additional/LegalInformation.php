<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "legal_information".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $text
 *
 * @property Users $user
 */
class LegalInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'legal_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Наименование',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalInformations()
    {
        return $this->hasMany(LegalInformation::className(), ['user_id' => 'id']);
    }

    /**
     * Boshlang'ich qiymatlarni qoshish
     */
    public function setDefaultValues($user_id)
    {
        $legal = new LegalInformation();
        $legal->user_id = $user_id;
        $legal->name = 'Юридическая информация';
        $legal->save();
    }
}
