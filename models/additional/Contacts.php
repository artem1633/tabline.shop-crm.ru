<?php

namespace app\models\additional;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $email
 * @property string $phone
 *
 * @property Users $user
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    public $search_name;
    public $search_email;
    public $search_phone;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'user_id' => 'Пользователь',
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'search_name' => 'Имя',
            'search_email' => 'E-mail',
            'search_phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\Users::className(), ['id' => 'user_id']);
    }

    public function searchContacts($params,$id, $post)
    {
        $query = Contacts::find()->where(['user_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
            ],
        ]);


        if(isset($post['Contacts']['search_name'])) {
            $query->andFilterWhere(['like', 'name', $post['Contacts']['search_name']]);
        }
        
        if(isset($post['Contacts']['search_email'])) {
            $query->andFilterWhere(['like', 'email', $post['Contacts']['search_email']]);
        }
        
        if(isset($post['Contacts']['search_phone'])) {
            $query->andFilterWhere(['like', 'phone', $post['Contacts']['search_phone']]);
        }
        

        return $dataProvider;
    }
}
