<?php

namespace app\models\additional;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\additional\Permissions;

/**
 * PermissionsSearch represents the model behind the search form about `app\models\additional\Permissions`.
 */
class PermissionsSearch extends Permissions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['view_user', 'delete_user', 'create_user', 'change_tariff', 'view_page', 'change_content_page', 'change_price_list', 'answer_to_questions', 'change_promocode', 'close_window', 'free_period', 'execute_order', 'unlock_registration'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Permissions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'view_user', $this->view_user])
            ->andFilterWhere(['like', 'delete_user', $this->delete_user])
            ->andFilterWhere(['like', 'create_user', $this->create_user])
            ->andFilterWhere(['like', 'change_tariff', $this->change_tariff])
            ->andFilterWhere(['like', 'view_page', $this->view_page])
            ->andFilterWhere(['like', 'change_content_page', $this->change_content_page])
            ->andFilterWhere(['like', 'change_price_list', $this->change_price_list])
            ->andFilterWhere(['like', 'answer_to_questions', $this->answer_to_questions])
            ->andFilterWhere(['like', 'change_promocode', $this->change_promocode])
            ->andFilterWhere(['like', 'close_window', $this->close_window])
            ->andFilterWhere(['like', 'free_period', $this->free_period])
            ->andFilterWhere(['like', 'execute_order', $this->execute_order])
            ->andFilterWhere(['like', 'unlock_registration', $this->unlock_registration]);

        return $dataProvider;
    }
}
